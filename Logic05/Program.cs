﻿using System;

namespace Logic05
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //contain();
            //padLeft();
            //convertArrayAll();
            Console.ReadKey();
        }

        static void convertArrayAll()
        {
            Console.WriteLine("---Convert Array All---");

            Console.Write("Masukan angka array(Pakai koma) = ");
            int[] array = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            Console.WriteLine(String.Join(" ",array));
        }

        static void padLeft()
        {
            Console.WriteLine("---Pad Left---");

            Console.Write("Masukan input = ");
            int input = int.Parse(Console.ReadLine());

            Console.Write("Masukan panjang karakter = ");
            int panjang = int.Parse(Console.ReadLine());

            Console.Write("Masukan Char = ");
            char chars = char.Parse(Console.ReadLine());

            Console.WriteLine($"Hasil Ped Left = {input.ToString().PadLeft(panjang, chars)}");
            

        }

        static void contain()
        {
            Console.WriteLine("---Contain---");

            Console.Write("Masukan Kalimat = ");
            string kalimat = Console.ReadLine();

            Console.Write("Masukan Contain = ");
            string contain = Console.ReadLine();

            if (kalimat.Contains(contain))
            {
                Console.WriteLine($"Kalimat ({kalimat}) Ini Mengandung {contain}");
            }
            else
            {
                Console.WriteLine($"Kalimat ({kalimat}) ini tidak mengandung {contain}");
            }

        }

    }
}
