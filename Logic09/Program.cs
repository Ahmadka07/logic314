﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Logic09
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soal1();
            //soal2();
            //soal3();
            //soal3Ver2();
            Console.ReadKey();
        }

        static void soal1()
        {
            Console.WriteLine("---Soal Nomor 1---");

            double bensin = 2.5;
            double[] jarak = new double[4];
            jarak[0] = 2;
            jarak[1] = 0.5;
            jarak[2] = 1.5;
            jarak[3] = 0.3;
            double hasil = 0;
            double sum = 0;
            string tampung = "";

            Console.Write("Masukan input = ");
            int input = int.Parse(Console.ReadLine());

            if (input > 0 && input <= 4)
            {
                for (int i = 0; i < input; i++)
                {
                    sum += jarak[i];
                    if ( i == 0)
                    {
                        tampung += jarak[i].ToString() + "Km";
                    }
                    else
                    {
                        tampung += " + " + jarak[i].ToString() + "Km";
                    }
                }
                Console.WriteLine($"Jarak tempuh = {tampung} = {sum}Km");
                hasil = sum / bensin;
                Console.WriteLine($"Bensin = {Math.Round((hasil), MidpointRounding.ToPositiveInfinity)} Liter");
            }
            else
            {
                Console.WriteLine("Invalid");
            }
        }

        static void soal2()
        {
            Console.WriteLine("---Soal Nomor 2---");

            Console.Write("Masukan jumlah kue pukis yang akan dibuat = ");
            int jml = int.Parse(Console.ReadLine());

            double terigu = 7.66;
            double gulaPasir = 12.66;
            double susu = 6.66;

            Console.WriteLine($"Bahan yang dibutuhkan untuk membuat {jml} kue pukis");

            double terigu2 = terigu * jml;
            double gulaPasir2 = gulaPasir * jml;
            double susu2 = susu * jml;

            Console.WriteLine($"{Math.Round(terigu2)}gr terigu");
            Console.WriteLine($"{Math.Round(gulaPasir2)}gr gula pasir");
            Console.WriteLine($"{Math.Round(susu2)}ml susu");

        }

        static void soal3()
        {
            Console.WriteLine("---Soal Nomor 3---");

            Console.Write("Masukan jumlah baris = ");
            int n = int.Parse(Console.ReadLine());
            //int n = 7;
            int o = 2;
            int sum = 2;
            int sum2 = 2;

            Console.Write("\n");

            for (int i = 0; i < n; i ++)
            {
                if (i < n - 1)
                {
                    for (int j = 0; j < n; j++)
                    {
                        if (j == 0)
                        {
                            Console.Write($"{o}\t");
                        }
                        else if (i == j)
                        {

                            sum += o;
                            Console.Write($"{sum}\t");
                        }
                        else
                        {
                            Console.Write("\t");
                        }
                    }
                    Console.Write("\n");
                }
                else
                {
                    for (int k = 0; k < n; k++)
                    {
                        if (k == 0)
                        {
                            Console.Write($"{o}\t");
                        }
                        else if (k > 0)
                        {
                            sum2 += o;
                            Console.Write($"{sum2}\t");
                        }
                    }
                }
            }
        }

        static void soal3Ver2()
        {
            Console.WriteLine("---Soal Nomor 3 Versi 2---");

            Console.Write("Masukan banyak suku = ");
            int suku = int.Parse(Console.ReadLine());
            Console.Write("Masukan angka awal = ");
            int angka = int.Parse(Console.ReadLine());

            for (int i = 0; i < suku; i++)
            {
                int x = 0;
                for (int j = 0; j < suku; j++)
                {
                    x += angka;
                    if (j == 0 || i == suku - 1 || i == j)
                    {
                        Console.Write(x.ToString() + "\t");
                    }
                    else
                    {
                        Console.Write("\t");
                    }
                }
                Console.WriteLine();
            }

        }

    }
}
