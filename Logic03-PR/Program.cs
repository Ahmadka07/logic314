﻿using System;

namespace Logic03_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "PR";

            //tugas1();
            //tugas2();
            //tugas3();
            //tugas4();
            //tugas5();
            //tugas6();
            //tugas7();
            //tugas8();

            Console.ReadKey();
        }

        static void tugas1()
        {
            Console.WriteLine("---Grade Nilai---");
            int nilai;
            int maxNilai = 100;

            Console.Write("Masukan Nilai = ");
            nilai = int.Parse(Console.ReadLine());

            if (nilai >= 90 && nilai <= maxNilai)
            {
                Console.WriteLine("Grade A");
            }
            else if (nilai >= 70 && nilai < 90)
            {
                Console.WriteLine("Grade B");
            }
            else if (nilai >= 50 && nilai < 70)
            {
                Console.WriteLine("Grade C");
            }
            else if (nilai >= 0 && nilai < 50)
            {
                Console.WriteLine("Grade E");
            }
            else
            {
                Console.WriteLine("Masukan Nilai yang Benar");
            }
        }

        static void tugas2()
        {
            Console.WriteLine("---Pembelian Pulsa---");

            int pulsa;
            int point = 0;

            Console.Write("Masukan jumlah pulsa yang dibeli = ");
            pulsa = int.Parse(Console.ReadLine());

            if (pulsa > 0)
            {
                if (pulsa >= 10000 && pulsa < 25000)
                {
                    point = 80;
                }
                else if (pulsa >= 25000 && pulsa < 50000)
                {
                    point = 200;
                }
                else if (pulsa >= 50000 && pulsa < 100000)
                {
                    point = 400;
                }
                else if (pulsa >= 100000)
                {
                    point = 800;
                }

                Console.WriteLine($"Pulsa = {pulsa}");
                Console.WriteLine($"Kamu mendapatkan = {point} poin");
            }
            else
            {
                Console.WriteLine("Invalid");
            }

            //string ternary = pulsa < 0 ? "Invalid" : pulsa.ToString();
            
            //Console.WriteLine($"Pulsa = {ternary}");
            //Console.WriteLine($"Kamu mendapatkan = {point} poin");

        }

        static void tugas3()
        {
            double diskonGrabFood = 0.4;
            string kodePromo = "JKTOVO";
            int minimumPembelanjaan = 30000;
            int ongkosKirimPer5km = 5000;
            int ongkosKirimLebihDari5km = 1000;

            int belanja, jarak, ongkir = 0;
            string promo;
            double diskon = 0, totalBelanja = 0;

            Console.Write("Masukan total belanja = ");
            belanja = int.Parse(Console.ReadLine());
            Console.Write("Masukan jarak = ");
            jarak = int.Parse(Console.ReadLine());
            Console.Write("Masukan kode promo = ");
            promo = Console.ReadLine();
            Console.WriteLine();

            if (belanja >= minimumPembelanjaan)
            {
                if (promo == kodePromo)
                {
                    diskon = belanja * diskonGrabFood;
                }
            }

            if (diskon > minimumPembelanjaan)
            {
                diskon = minimumPembelanjaan;
            }

            if (jarak > 5)
            {
                //int tampungBagi = (jarak / 5) * ongkosKirimPer5km;
                //int tampungMod = (jarak % 5) * ongkosKirimLebihDari5km;
                ongkir = ongkosKirimPer5km + ((jarak - 5) * ongkosKirimLebihDari5km);
            }
            else
            {
                ongkir = ongkosKirimPer5km;
            }

            if (belanja > 0 && jarak > 0)
            {
                Console.WriteLine($"Belanja : {belanja}");
                Console.WriteLine($"Diskon 40% = {diskon}");
                Console.WriteLine($"Ongkir : {ongkir}");
                Console.WriteLine($"Total Belanja : {totalBelanja = belanja - diskon + ongkir}");
            }
            else
            {
                Console.WriteLine("Perhatikan format Inputan!");
            }

            /*if (belanja >= 30000)
            {
                Console.WriteLine($"Belanja : {belanja}");
                diskon = belanja * diskonGrabFood;

                if (promo == kodePromo)
                {

                    if (diskon > 0 && diskon <= 30000)
                    {
                        Console.WriteLine($"Diskon 40% : {diskon}");
                    }

                    else
                    {
                        diskon = 30000;
                        Console.WriteLine($"Diskon 40% : {diskon}");
                    }
                }
                else
                {
                    diskon = 0;
                    Console.WriteLine($"Diskon 40% : {diskon}");
                }

                if (jarak > 0 && jarak < 5)
                {
                    ongkir = ongkosKirimPer5km;
                    Console.WriteLine($"Ongkir : {ongkir}");
                }
                else
                    {
                        int tampungBagi, tampungMod;
                        tampungBagi = (jarak / 5) * ongkosKirimPer5km;
                        tampungMod = (jarak % 5) * ongkosKirimLebihDari5km;
                        ongkir = tampungBagi + tampungMod;
                        Console.WriteLine($"Ongkir : {ongkir}");
                    }

                totalBelanja = belanja + ongkir - diskon;
                Console.WriteLine($"Total belanja : {totalBelanja}");
            
            }

            else if (belanja > 0 && belanja < minimumPembelanjaan && jarak < 5)
            {
                diskon = belanjaKurangDari30ribu;
                Console.WriteLine($"Belanja : {belanja}");
                Console.WriteLine($"Diskon 40% : {diskon}");
                ongkir = ongkosKirimPer5km;
                Console.WriteLine($"Ongkir : {ongkir}");
                totalBelanja = belanja + ongkir - diskon;
                Console.WriteLine($"Total belanja : {totalBelanja}");
            }

            else if (belanja > 0 && belanja < minimumPembelanjaan && jarak > 5)
            {
                diskon = belanjaKurangDari30ribu;
                Console.WriteLine($"Belanja : {belanja}");
                Console.WriteLine($"Diskon 40% : {diskon}");
                int tampungBagi, tampungMod;
                tampungBagi = (jarak / 5) * ongkosKirimPer5km;
                tampungMod = (jarak % 5) * ongkosKirimLebihDari5km;
                ongkir = tampungBagi + tampungMod;
                Console.WriteLine($"Ongkir : {ongkir}");
                totalBelanja = belanja + ongkir - diskon;
                Console.WriteLine($"Total belanja : {totalBelanja}");
            }

            else
            {
                Console.WriteLine("Perhatikan format inputan dengan benar");
            }*/
        }

        static void tugas4()
        {
            int minOrder1 = 30000;
            int minOrder2 = 50000;
            int minOrder3 = 100000;
            int ongkir1 = 5000;
            int ongkir2 = 10000;
            int ongkir3 = 20000;
            int potBelanja1 = 5000;
            int potBelanja2 = 10000;
            int potBelanja3 = 10000;
            int ongkir = 0, potBelanja = 0, totalBelanja;

            Console.Write("Belanja : ");
            int belanja = int.Parse(Console.ReadLine());
            Console.Write("Ongkos kirim : ");
            int ongkosKirim = int.Parse(Console.ReadLine());
            Console.Write("Pilih voucher : ");
            string voucher = Console.ReadLine();

            Console.WriteLine();

            if (voucher == "1" && belanja >= minOrder1)
            {
                
                    ongkir = ongkir1;
                    potBelanja = potBelanja1;
             
            }
            else if (voucher == "2" && belanja >= minOrder2)
            {
                
                
                    ongkir = ongkir2;
                    potBelanja = potBelanja2;
                
            }
            else if (voucher == "3" && belanja >= minOrder3)
            {
                
                
                    ongkir = ongkir3;
                    potBelanja = potBelanja3;
                
            }

            if (belanja > 0 && ongkosKirim > 0)
            {
                Console.WriteLine($"Belanja : {belanja}");
                Console.WriteLine($"Ongkos Kirim : {ongkosKirim}");
                Console.WriteLine($"Diskon ongkir : {ongkir}");
                Console.WriteLine($"Diskon belanja : {potBelanja}");
                Console.WriteLine($"Total Belanja : {totalBelanja = belanja + ongkosKirim - ongkir - potBelanja}");
            }
            else
            {
                Console.WriteLine("Perhatikan format Inputan!");
            }
        }

        static void tugas5()
        {
            string generasiBabyBoomer = "Baby boomer";
            string generasiX = "Generasi X";
            string generasiY = "Generasi Y";
            string generasiZ = "Generasi Z";

            Console.Write("Masukan nama anda : ");
            string nama = Console.ReadLine();
            Console.Write("Tahun berapa anda lahir : ");
            int tahunLahir = int.Parse(Console.ReadLine());

            if (tahunLahir >= 1944 && tahunLahir < 1965)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong {generasiBabyBoomer}");
            }
            else if (tahunLahir >= 1965 && tahunLahir < 1980)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong {generasiX}");
            }
            else if (tahunLahir >= 1980 && tahunLahir < 1995)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong {generasiY}");
            }
            else if (tahunLahir >= 1995 && tahunLahir <= 2015)
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda tergolong {generasiZ}");
            }
            else
            {
                Console.WriteLine($"{nama}, berdasarkan tahun lahir anda belum tergolong dari generasi yang diketahui");
            }
        }

        static void tugas6()
        {
            int tunjangan, gapok, banyakBulan;
            string nama;
            double pajak = 0;
            double bpjs = 0;
            double totalGajiPerBanyakBulan = 0;

            Console.Write("Nama : ");
            nama = Console.ReadLine();
            Console.Write("Tunjuangan : ");
            tunjangan = int.Parse(Console.ReadLine());
            Console.Write("Gapok : ");
            gapok = int.Parse(Console.ReadLine());
            Console.Write("Banyak Bulan : ");
            banyakBulan = int.Parse(Console.ReadLine());
            Console.Write("\n");
            int gaptun = gapok + tunjangan;

            if (gapok > 0 && tunjangan >= 0)
            {
                if (gaptun > 0 && gaptun <= 5000000)
                {
                    pajak = 0.05;
                }
                else if (gaptun <= 10000000)
                {
                    pajak = 0.10;
                }
                else if (gaptun > 10000000)
                {
                    pajak = 0.15;
                }
                Console.WriteLine($"Karyawan atas nama {nama} slip gaji sebagai berikut : ");
                Console.WriteLine($"Pajak : {pajak *= gaptun}");
                Console.WriteLine($"bpjs : {bpjs = 0.03 * gaptun}");
                Console.WriteLine($"Gaji/bln : {totalGajiPerBanyakBulan = gaptun - (pajak + bpjs)}");
                Console.WriteLine($"Total gaji/Banyak bulan : {totalGajiPerBanyakBulan * banyakBulan}");
            }
            else
            {
                Console.WriteLine("Invalid");
            }
        }

        static void tugas7()
        {
            double beratBadan, tinggiBadan, bmi;

            Console.Write("Masukan berat badan anda (kg) : ");
            beratBadan = double.Parse(Console.ReadLine());
            Console.Write("Masukan tinggi badan (cm) : ");
            tinggiBadan = double.Parse(Console.ReadLine());
            tinggiBadan /= 100;
            tinggiBadan *= tinggiBadan;
            bmi = beratBadan / tinggiBadan;
            Console.WriteLine($"Nilai BMI anda adalah : {Math.Round(bmi, 4)}");

            if (bmi < 18.5)
            {
                Console.WriteLine("Anda termasuk berbadan kurus");
            }
            else if (bmi >= 18.5 && bmi < 25)
            {
                Console.WriteLine("Anda termasuk berbadan sehat/langsing");
            }
            else if (bmi >= 25)
            {
                Console.WriteLine("Anda termasuk berbadan gemuk");
            }
        }

        static void tugas8()
        {
            double mtk, fisika, kimia, rataRata;

            Console.Write("Masukan Nilai MTK : ");
            mtk = double.Parse(Console.ReadLine());
            Console.Write("Masukan Nilai Fisika : ");
            fisika = double.Parse(Console.ReadLine());
            Console.Write("Masukan Nilai Kimia : ");
            kimia = double.Parse(Console.ReadLine());
            rataRata = Math.Round((mtk + fisika + kimia) / 3, 0);
            
            if (rataRata > 0)
            {
                Console.WriteLine($"Nilai Rata-Rata = {rataRata}");
                if (rataRata >= 50)
                {
                    Console.WriteLine("Selamat");
                    Console.WriteLine("Kamu Berhasil");
                    Console.WriteLine("Kamu Hebat");
                }
                else if (rataRata > 0 && rataRata < 50)
                {
                    Console.WriteLine("Maaf");
                    Console.WriteLine("Kamu Gagal");
                }
            }
            else
            {
                Console.WriteLine("Invalid");
            }
        }

    }
}
