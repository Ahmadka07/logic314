﻿using System;
using System.ComponentModel.Design;
using System.Linq;

namespace Dimension_And_Case_Study
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Dimension & Case Study";
            //soal01();
            //soal02();
            //soal03();
            //soal04();
            //soal05();
            //soal06();
            //soal07();
            //soal08();
            //soal09();
            //soal10();
            //soal11();
            //soal12();
            //soal13();
            //soal14();
            //soal15();
            //soal16();
            //soal17();
            //soal18();
            //soal19();
            //soal20();
            //soal21();
            //soal22();
            //soal23();
            //soal24();
            //superReduceString();
            //jumlahBilanganPrima();
            //bilanganPrima();
            //kalimatPalindrom();
            Console.ReadKey();
        }

        static void soal01()
        {
            Console.WriteLine("Soal Nomor 1");
            Console.WriteLine("Contoh Output : n = 7 || 1 3 5 7 9 11 13");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            int isiArray = 1;

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = isiArray;
                Console.Write(array[i] + " ");
                isiArray += 2;
            }
        }

        static void soal02()
        {
            Console.WriteLine("Soal Nomor 2");
            Console.WriteLine("Contoh Output : n = 7 || 2 4 6 8 10 12 14");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            int isiArray = 2;

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = isiArray;
                Console.Write(array[i] + " ");
                isiArray += 2;
            }
        }

        static void soal03()
        {
            Console.WriteLine("Soal Nomor 3");
            Console.WriteLine("Contoh Output : n = 7 || 1 4 7 10 13 16 19");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            int isiArray = 1;

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = isiArray;
                Console.Write(array[i] + " ");
                isiArray += 3;
            }
        }

        static void soal04()
        {
            Console.WriteLine("Soal Nomor 4");
            Console.WriteLine("Contoh Output : n = 7 || 1 5 9 13 17 21 25");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            int isiArray = 1;

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = isiArray;
                Console.Write(array[i] + " ");
                isiArray += 4;
            }
        }

        static void soal05()
        {
            Console.WriteLine("Soal Nomor 5");
            Console.WriteLine("Contoh Output : n = 7 || 1 5 * 9 13 * 17");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            int isiArray = 1;
            int tampung = 2;

            for (int i = 0; i < array.Length; i++)
            {
                
                array[i] = isiArray;
                if (i == tampung)
                {
                    Console.Write("* ");
                    tampung += 2;
                }
                Console.Write(array[i] + " ");
                isiArray += 4;
            }
        }

        static void soal06()
        {
            Console.WriteLine("Soal Nomor 6");
            Console.WriteLine("Contoh Output : n = 7 || 1 5 * 13 17 * 25");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            int isiArray = 1;
            int tampung = 2;


            for (int i = 0; i < array.Length; i++)
            {
                array[i] = isiArray;
                if (i == tampung)
                {
                    Console.Write("* ");
                    tampung += 3;
                }
                else
                {
                    Console.Write(array[i] + " ");
                }
                isiArray += 4;
            }
        }

        static void soal07()
        {
            Console.WriteLine("Soal Nomor 7");
            Console.WriteLine("Contoh Output : n = 7 || 2 4 8 16 32 64 128");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            int isiArray = 2;

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = isiArray;
                Console.Write(array[i] + " ");
                isiArray += isiArray; 
            }
        }

        static void soal08()
        {
            Console.WriteLine("Soal Nomor 8");
            Console.WriteLine("Contoh Output : n = 7 || 3 9 27 81 243 729 2187");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            int isiArray = 3;

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = isiArray;
                Console.Write(array[i] + " ");
                isiArray *= 3;
            }
        }

        static void soal09()
        {
            Console.WriteLine("Soal Nomor 9");
            Console.WriteLine("Contoh Output : n = 7 || 4 16 * 64 256 * 1024");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            int isiArray = 4;
            int tampung = 2;

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = isiArray;
                if (i == tampung)
                {
                    Console.Write("* ");
                    tampung += 3;
                }
                else
                {
                    Console.Write(array[i] + " ");
                    isiArray *= 4;
                }
                
            }
        }

        static void soal10()
        {
            Console.WriteLine("Soal Nomor 10");
            Console.WriteLine("Contoh Output : n = 7 || 3 9 27 XXX 243 729 2187");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            int isiArray = 3;
            int tampung = 3;

            for (int i = 0; i < array.Length; i++)
            {
                array[i] = isiArray;
                if (i == tampung)
                {
                    Console.Write("XXX ");
                    tampung += 4;
                }
                else
                {
                    Console.Write(array[i] + " ");
                }
                isiArray *= 3;
            }
        }

        // 2 Dimension Array

        static void soal11()
        {
            Console.WriteLine("Soal Nomor 11");
            Console.WriteLine("Contoh Output = n = 7, n = 3");
            Console.WriteLine("I : 0 1 2 3 4 5 6");
            Console.WriteLine("O : 1 3 9 27 81 243 729");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("Masukan Nilai N2 = ");
            int n2 = int.Parse(Console.ReadLine());
            int tampung = 1;
            Console.Write("\n");

            int[,] array2Dimension = new int[2, n];

            for (int i = 0; i < array2Dimension.Rank; i++)
            {
                if (i == 0)
                {
                    Console.Write("I : ");
                    for (int j = 0; j < n; j++)
                    {
                        array2Dimension[i, j] = j;
                        Console.Write(array2Dimension[i, j] + "\t");
                    }
                }
                else
                {
                    Console.Write("O : ");
                    for (int j = 0; j < n; j++)
                    {
                        array2Dimension[i, j] = tampung;
                        Console.Write(array2Dimension[i, j] + "\t");
                        tampung *= n2;
                    }
                }
                Console.Write("\n");
            }
        }

        static void soal12()
        {
            Console.WriteLine("Soal Nomor 12");
            Console.WriteLine("Contoh Output : n = 7, n2 = 3");
            Console.WriteLine("0\t1\t2\t3\t4\t5\t6");
            Console.WriteLine("1\t3\t-9\t27\t81\t-243\t729");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("Masukan Nilai N2 = ");
            int n2 = int.Parse(Console.ReadLine());
            int[,] array2Dimension = new int[2, n];
            int tampung = 1;
            int tampung2 = 2; 

            for (int i = 0; i < array2Dimension.Rank; i++)
            {
                if (i == 0)
                {
                    for (int j = 0; j < n; j++)
                    {
                        array2Dimension[i, j] = j;
                        Console.Write(array2Dimension[i,j] + "\t");
                    }
                    Console.Write("\n");
                }
                else
                {
                    for (int k = 0; k < n; k++)
                    {
                        if (k == tampung2)
                        {
                            array2Dimension[i, k] = -tampung;
                            Console.Write(array2Dimension[i, k] + "\t");
                            tampung *= n2;
                            tampung2 += 3;
                        }
                        else
                        {
                            array2Dimension[i, k] = tampung;
                            Console.Write(array2Dimension[i, k] + "\t");
                            tampung *= n2;
                        }
                    }
                }
                
            }
        }

        static void soal13()
        {
            Console.WriteLine("Soal Nomor 13");
            Console.WriteLine("Contoh Output : n = 7, n2 = 3");
            Console.WriteLine("0\t1\t2\t3\t4\t5\t6");
            Console.WriteLine("3\t6\t12\t24\t12\t6\t3");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("Masukan Nilai N2 (bernilai ganjil) = ");
            int n2 = int.Parse(Console.ReadLine());
            int[,] array2Dimension = new int[2, n];
            int tampung = n2;

            if ((n2 + 1) % 2 == 0)
            {
                for (int i = 0; i < array2Dimension.Rank; i++)
                {
                    if (i == 0)
                    {
                        for (int j = 0; j < n; j++)
                        {
                            array2Dimension[i, j] = j;
                            Console.Write(array2Dimension[i, j] + "\t");
                        }
                        Console.Write("\n");
                    }
                    else
                    {
                        for (int k = 0; k < n; k++)
                        {
                            if (k >= 0 && k < (n - 1) / 2)
                            {
                                array2Dimension[i, k] = tampung;
                                Console.Write(array2Dimension[i, k] + "\t");
                                tampung *= 2;
                            }
                            else
                            {

                                array2Dimension[i, k] = tampung;
                                tampung /= 2;
                                Console.Write(array2Dimension[i, k] + "\t");

                            }
                        }
                    }
                }
            }
            else
            {
                Console.WriteLine("Invalid");
            }
        }

        static void soal14()
        {
            Console.WriteLine("Soal Nomor 14");
            Console.WriteLine("Contoh Output : n = 7, n2 = 5");
            Console.WriteLine("0\t1\t2\t3\t4\t5\t6");
            Console.WriteLine("1\t5\t2\t10\t3\t15\t4");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("Masukan Nilai N2 = ");
            int n2 = int.Parse(Console.ReadLine());
            int[,] array2Dimension = new int[2, n];
            int tampung = 1;
            int tampung2 = n2;

            for (int i = 0; i < array2Dimension.Rank; i++)
            {
                if (i == 0)
                {
                    for (int j = 0; j < n; j++)
                    {
                        array2Dimension[i, j] = j;
                        Console.Write(array2Dimension[i, j] + "\t");
                    }
                    Console.Write("\n");
                }
                else
                {
                    for (int k = 0; k < n; k++)
                    {
                        if (k % 2 == 0)
                        {
                            array2Dimension[i, k] = tampung;
                            Console.Write(array2Dimension[i, k] + "\t");
                            tampung++;
                        }
                        else
                        {
                            array2Dimension[i, k] = tampung2;
                            Console.Write(array2Dimension[i, k] + "\t");
                            tampung2 += n2;

                        }
                    }
                }
            }
        }

        static void soal15()
        {
            Console.WriteLine("Soal Nomor 15");
            Console.WriteLine("Contoh Output = n = 7");
            Console.WriteLine("I : 0 1 2 3 4 5 6");
            Console.WriteLine("0 : 7 8 9 10 11 12 13");
            Console.WriteLine("1 : 14 15 16 17 18 19 20");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("\n");
            int tampung = 0;

            int[,] array2Dimension = new int[3, n];

            //Console.WriteLine(array2Dimension.Rank); = 2

            for (int i = 0; i <= array2Dimension.Rank; i++)
            {
                if (i == 0)
                {
                    Console.Write("I :\t");
                }
                else if (i == 1)
                {
                    Console.Write("0 :\t");
                }
                else
                {
                    Console.Write("1 :\t");
                }
                for (int j = 0; j < n; j++)
                {   
                    array2Dimension[i, j] = tampung;
                    Console.Write(array2Dimension[i, j] + "\t");
                    tampung++;
                }
                Console.Write("\n"); 
            }
        }

        static void soal16()
        {
            Console.WriteLine("Soal Nomor 16");
            Console.WriteLine("Contoh Output = n = 7");
            Console.WriteLine("0 1 2 3 4 5 6");
            Console.WriteLine("1 7 49 343 2401 16807 117649");
            Console.WriteLine("1 8 51 346 2405 16812 117655");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("\n");
            int tampung = 0;
            int tampung2 = 1;
            int tampung3 = 1;

            int[,] array2Dimension = new int[3, n];

            //Console.WriteLine(array2Dimension.Rank); = 2

            for (int i = 0; i <= array2Dimension.Rank; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == 0)
                    {
                        array2Dimension[i, j] = tampung;
                        Console.Write(array2Dimension[i, j] + "\t");
                        tampung++;
                    }
                    else if (i == 1)
                    {
                        array2Dimension[i, j] = tampung2;
                        Console.Write(array2Dimension[i, j] + "\t");
                        tampung2 *= n;
                    }
                    else
                    {
                        tampung3 = array2Dimension[i - 1, j] + j;
                        array2Dimension[i, j] = tampung3;
                        Console.Write(array2Dimension[i, j] + "\t");
                        
                    }
                }
                Console.Write("\n");
            }
        }

        static void soal17()
        {
            Console.WriteLine("Soal Nomor 17");
            Console.WriteLine("Contoh Output = n = 7");
            Console.WriteLine("I : 0 1 2 3 4 5 6");
            Console.WriteLine("0 : 7 8 9 10 11 12 13");
            Console.WriteLine("1 : 14 15 16 17 18 19 20");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("\n");
            int tampung = 0;

            int[,] array2Dimension = new int[3, n];

            //Console.WriteLine(array2Dimension.Rank); = 2

            for (int i = 0; i <= array2Dimension.Rank; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    array2Dimension[i, j] = tampung;
                    Console.Write(array2Dimension[i, j] + "\t");
                    tampung++;
                }
                Console.Write("\n");
            }
        }

        static void soal18()
        {
            Console.WriteLine("Soal Nomor 18");
            Console.WriteLine("Contoh Output = n = 7");
            Console.WriteLine("0 1 2 3 4 5 6");
            Console.WriteLine("0 2 4 6 8 10 12");
            Console.WriteLine("0 3 6 9 12 15 18");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("\n");
            int tampung = 0;
            int tampung2 = 0;
            int tampung3 = 0;

            int[,] array2Dimension = new int[3, n];

            //Console.WriteLine(array2Dimension.Rank); = 2

            for (int i = 0; i <= array2Dimension.Rank; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == 0)
                    {
                        array2Dimension[i, j] = tampung;
                        Console.Write(array2Dimension[i, j] + "\t");
                        tampung++;
                    }
                    else if (i == 1)
                    {
                        array2Dimension[i, j] = tampung2;
                        Console.Write(array2Dimension[i, j] + "\t");
                        tampung2 += i+1;
                    }
                    else
                    {
                        tampung3 = array2Dimension[i - 1, j] + j;
                        array2Dimension[i, j] = tampung3;
                        Console.Write(array2Dimension[i, j] + "\t");
                    }
                }
                Console.Write("\n");
            }
        }

        static void soal19()
        {
            Console.WriteLine("Soal Nomor 19");
            Console.WriteLine("Contoh Output = n = 7, n2 = 3");
            Console.WriteLine("0 1 2 3 4 5 6");
            Console.WriteLine("0 3 6 9 12 15 18");
            Console.WriteLine("18 15 12 9 6 3 0");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("Masukan Nilai N2 = ");
            int n2 = int.Parse(Console.ReadLine());
            Console.Write("\n");
            int tampung = 0;
            int tampung2 = 0;

            int[,] array2Dimension = new int[3, n];

            //Console.WriteLine(array2Dimension.Rank); = 2

            for (int i = 0; i <= array2Dimension.Rank; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == 0)
                    {
                        array2Dimension[i, j] = tampung;
                        Console.Write(array2Dimension[i, j] + "\t");
                        tampung++;
                    }
                    else if (i == 1)
                    {
                        array2Dimension[i, j] = tampung2;
                        Console.Write(array2Dimension[i, j] + "\t");
                        tampung2 += n2;
                    }
                    else
                    {
                        //tampung3 = array2Dimension[i - 1, j] + j;
                        //array2Dimension[i, j] = tampung3;
                        Console.Write(array2Dimension[i - 1, n - 1 - j] + "\t");
                    }
                }
                Console.Write("\n");
            }
        }

        static void soal20()
        {
            Console.WriteLine("Soal Nomor 20");
            Console.WriteLine("Contoh Output = n = 7, n2 = 3");
            Console.WriteLine("0 1 2 3 4 5 6");
            Console.WriteLine("0 3 6 9 12 15 18");
            Console.WriteLine("0 4 8 12 16 20 24");
            Console.Write("\n");
            Console.Write("Masukan Nilai N = ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("Masukan Nilai N2 = ");
            int n2 = int.Parse(Console.ReadLine());
            Console.Write("\n");
            int tampung = 0;
            int tampung2 = 0;
            int tampung3 = 0;

            int[,] array2Dimension = new int[3, n];

            //Console.WriteLine(array2Dimension.Rank); = 2

            for (int i = 0; i <= array2Dimension.Rank; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == 0)
                    {
                        array2Dimension[i, j] = tampung;
                        Console.Write(array2Dimension[i, j] + "\t");
                        tampung++;
                    }
                    else if (i == 1)
                    {
                        array2Dimension[i, j] = tampung2;
                        Console.Write(array2Dimension[i, j] + "\t");
                        tampung2 += n2;
                    }
                    else
                    {
                        tampung3 = array2Dimension[i - 1, j] + j;
                        array2Dimension[i, j] = tampung3;
                        Console.Write(array2Dimension[i, j] + "\t");
                    }
                }
                Console.Write("\n");
            }
        }

        // String

        static void soal21()
        {
            Console.WriteLine("Soal Nomor 21");
            Console.WriteLine("Contoh Output : s = (saveChangesInTheEditor)");
            Console.WriteLine("Print = 5");
            Console.Write("\n");

            int hasil = 0;
            int hasil2 = 0;
            string s = "saveChangesInTheEditor";
            char[] array = s.ToCharArray();

            for (int i = 0; i < array.Length; i++)
            {
                if (i == 0 || char.IsUpper(array[i]))
                {
                    hasil++;
                }
            }

            for (int j = 0; j < s.Length; j++)
            {
                if (j == 0 || char.IsUpper(s[j]))
                {
                    hasil2++;
                }
            }
            Console.WriteLine(hasil);
            Console.WriteLine(hasil2);
        }

        static void soal22()
        {
            Console.WriteLine("Soal Nomor 22");
            Console.WriteLine("Contoh Output : 3 || Abi, 11 || #HackRank");
            Console.Write("\n");

            Console.Write("Masukan String = ");
            string password = Console.ReadLine();
            //string password = "#HackerRank";
            int n = password.Length;

            char[] array = password.ToCharArray();

            Console.WriteLine();
            Console.WriteLine($"Panjang password = {password.Length}");

            string numbers = "0123456789"; 
            string lower_case = "abcdefghijklmnopqrstuvwxyz"; 
            string upper_case = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"; 
            string special_characters = "!@#$%^&*()-+";

            int hasil = 0;
            int no = 1;  int lower = 1; int upper = 1; int special = 1;
            for (int i = 0; i < n; i ++)
            {
                if (numbers.Contains(array[i]) && no == 1)
                {
                    no = 0;
                }
                else if (lower_case.Contains(array[i]) && lower == 1)
                {
                    lower = 0;
                }
                else if (upper_case.Contains(array[i]) && upper == 1)
                {
                    upper = 0;
                }
                else if (special_characters.Contains(array[i]) && special == 1)
                {
                    special = 0;
                }
            }

            hasil = Math.Max(6 - n, (no + lower + upper + special));

            Console.WriteLine(hasil);
            
            /*
            if (!password.Intersect(numbers).Any())
            {
                hitung++;
            }
            if (!password.Intersect(lower_case).Any())
            {
                hitung++;
            }
            if (!password.Intersect(upper_case).Any())
            {
                hitung++;
            }
            if (!password.Intersect(special_characters).Any())
            {
                hitung++;
            }

            int hitung2 = Math.Max(6 - n, hitung);

            Console.Write("\n");
            Console.WriteLine(hitung);

            int hitung3 = 0;

            if (!password.Any(Char.IsDigit))
            {
                hitung3++;
            }
            if (!password.Any(Char.IsLower))
            {
                hitung3++;
            }
            if (!password.Any(Char.IsUpper))
            {
                hitung3++;
            }
            if (!password.Any(Char.IsPunctuation))
            {
                hitung3++;
            }

            int hitung4 = Math.Max(6 - n, hitung3);

            Console.Write("\n");
            Console.WriteLine(hitung4);
            */

            /*
            int idxn = Array.IndexOf(array, numbers);
            int idxl = Array.IndexOf(array, lower_case);
            int idxu = Array.IndexOf(array, upper_case);
            int idxs = Array.IndexOf(array, special_characters);

            Console.WriteLine(idxn);

            if (n <= 6)
            {
                hitung = n;
            }
            else
            {
                for (int i = 0; i < n; i++)
                {
                    for (int j = 0; j < arrayNumbers.Length; j++)
                    {
                        if ()
                        {

                        }
                    }
                }
            }*/
        }

        static void soal23()
        {
            Console.WriteLine("Soal Nomor 23");
            Console.WriteLine("Contoh Output : Original alphabet  = a,b,c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z");
            Console.WriteLine("------------- : alphabet rotated 2 = c,d,e,f,g,h,i,j,k,l,m,n,o,p,q,r,s,t,u,v,w,x,y,z,a,b");
            Console.WriteLine("------------- : 11 || middle-Outz || 2");
            Console.WriteLine("------------- : okffng-Qwvb");
            Console.Write("\n");
            string s = "middle-Outz";
            string alphabet = "abcdefghijklmnopqrstuvwxyz";
            int k = s.Length;
            int rotate = 2;
            char[] array = s.ToCharArray();
            char[] arrayAlphabet = alphabet.ToCharArray();
            char[] arrayRotateAlphabet = alphabet.ToCharArray();
            string hasil = "";

            for (int i = 0; i < rotate; i++)
            {
                char tampung = arrayRotateAlphabet[0];
                for (int j = 0; j < arrayRotateAlphabet.Length - 1; j++)
                {
                    arrayRotateAlphabet[j] = arrayRotateAlphabet[j + 1];
                }
                arrayRotateAlphabet[arrayRotateAlphabet.Length - 1] = tampung;
            }

            for (int i = 0; i < array.Length; i++)
            {
                for (int j = 0; j < arrayAlphabet.Length; j++)
                {
                    if (Char.IsLower(array[i]))
                    {
                        if (array[i] == arrayAlphabet[j])
                        {
                            hasil += arrayRotateAlphabet[j];
                        }
                    }
                    else if (Char.IsUpper(array[i]))
                    {
                        char tampung = Char.ToLower(array[i]);
                        if (tampung == arrayAlphabet[j])
                        {
                            hasil += Char.ToUpper(arrayRotateAlphabet[j]);
                        }
                    }
                    else
                    {
                        hasil += array[i];
                        break;
                    }

                }
            }

            Console.WriteLine(hasil);

            /*
            char[] array = s.ToCharArray();
            char[] arrayAlphabet = alphabet.ToCharArray();
            string rotateAlphabet = alphabet.Substring(2) + alphabet.Substring(0, rotate);
            char[] arrayRotateAlphabet = rotateAlphabet.ToCharArray();
            string hasil = "";

            for (int i = 0; i < k; i++)
            {
                for (int j = 0; j < arrayAlphabet.Length; j++)
                {
                    if (Char.IsLower(array[i]))
                    {
                        if (array[i] == arrayAlphabet[j])
                        {
                            hasil += arrayRotateAlphabet[j];
                        }
                    }
                    else if (Char.IsUpper(array[i]))
                    {
                        char tampung = Char.ToLower(array[i]);
                        if (tampung == arrayAlphabet[j])
                        {
                            hasil += Char.ToUpper(arrayRotateAlphabet[j]);
                        }
                    }
                    else
                    {
                        hasil += array[i];
                        break;
                    }
                    
                }
            }

            Console.WriteLine(hasil);
            */

        }

        static void soal24()
        {
            Console.WriteLine("Soal Nomor 24");
            Console.WriteLine("Input  = SOSOOSOSOSOSOSSOSOSOSOSOSOS");
            Console.WriteLine("Output = 12");
            Console.WriteLine("Deteksi Kesalahan dari sinyal SOS, Contoh di atas terdapat 12 kali kesalahan");

            int hasil2 = 0;
            int hasil = 0;
            string s = "SOSOOSOSOSOSOSSOSOSOSOSOSOS";
            char[] sinyal = s.ToCharArray();

            for (int i = 0; i < sinyal.Length; i++)
            {
                if (sinyal[i] != 'S')
                {
                    hasil++;
                }
                if (sinyal[i + 1] != 'O')
                {
                    hasil++;
                }
                if (sinyal[i + 2] != 'S')
                {
                    hasil++;
                }

                if (sinyal[i] == 'S' && sinyal[i + 1] == 'O' && sinyal[i + 2] == 'S')
                {
                    hasil2++;
                }
                i += 2;
            }
            Console.WriteLine(hasil);
            Console.WriteLine(hasil2);
        }

        static void superReduceString()
        {
            string s = "aaabccddd";
            Console.WriteLine(s);
            string result = s;
            char[] array = s.ToCharArray();
            for (int i = 0; i < array.Length; i++)
            {
                result = result.Replace(array[i].ToString() + array[i].ToString(), "");
            }
            if (result.Equals(""))
            {
                result = "Empty String";
            }
            Console.WriteLine(result); 
        }

        static void jumlahBilanganPrima()
        {
            Console.WriteLine("---Bilangan Prima---");

            Console.Write("Masukan Jumlah bilangan = ");
            int bilangan = int.Parse(Console.ReadLine());
            bool prima = true;

            if (bilangan >= 2)
            {
                for (int i = 2; i <= bilangan; i++)
                {
                    for (int j = 2; j < i; j++)
                    {
                        if ((i % j) == 0)
                        {
                            prima = false;
                            break;
                        }
                    }
                    if (prima)
                    {
                        Console.Write($"{i} ");
                    }
                    prima = true;
                }
            }
            else
            {
                Console.WriteLine("Invalid");
            }            
        }

        static void bilanganPrima()
        {
            Console.WriteLine("---Bilangan Prima---");

            Console.Write("Masukan Jumlah bilangan = ");
            int bilangan = int.Parse(Console.ReadLine());
            bool prima = true;

            if (bilangan >= 2)
            {
                for (int j = 2; j < bilangan; j++)
                {
                    if ((bilangan % j) == 0)
                    {
                        prima = false;
                        break;
                    }
                }

                if (prima)
                {
                    Console.Write($"{bilangan} merupakan bilangan prima");
                }
                else
                {
                    Console.WriteLine($"{bilangan} bukan bilangan prima");
                }

                prima = true;
            }
            else
            {
                Console.WriteLine("Invalid");
            }
        }

        static void kalimatPalindrom()
        {
            Console.WriteLine("---Check Kalimat Palindrom--");

            Console.Write("Masukan Kalimat = \t");
            char[] huruf1 = Console.ReadLine().ToLower().ToCharArray();
            char[] huruf2 = new char[huruf1.Length];
            int isi = 0;
            bool pangaram = true;

            for (int i = huruf1.Length - 1; i >= 0; i--)
            {
                huruf2[isi] = huruf1[i];
                isi++;
            }

            for (int i = 0; i < huruf1.Length; i++)
            {
                if (huruf1[i] != huruf2[i])
                {
                    pangaram = false;
                    break;
                }
            }

            if (pangaram)
            {
                Console.WriteLine("Kalimat " + String.Join("", huruf1) + " termasuk palindrom");
            }
            else
            {
                Console.WriteLine("Kalimat " + String.Join("", huruf1) + " bukan palindrom");
            }
        }
    }
}
