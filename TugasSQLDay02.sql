CREATE DATABASE DB_Entertainer
USE DB_Entertainer

CREATE TABLE artis (
kd_artis VARCHAR(100) PRIMARY KEY NOT NULL,
nm_artis VARCHAR(100) NOT NULL,
jk VARCHAR(100) NOT NULL,
bayaran BIGINT NOT NULL,
award INT,
negara VARCHAR(100)
)

INSERT INTO artis (kd_artis, nm_artis, jk, bayaran, award, negara)
VALUES
('A001', 'ROBERT DOWNEY JR', 'PRIA', 3000000000, 2, 'AS'),
('A002', 'ANGELINA JOLIE', 'WANITA', 700000000, 1, 'AS'),
('A003', 'JACKIE CHAN', 'PRIA', 200000000, 7, 'HK'),
('A004', 'JOE TASLIM', 'PRIA', 350000000, 1, 'ID'),
('A005', 'CHELSEA ISLAN', 'WANITA', 300000000, 0, 'ID')

SELECT * FROM artis

CREATE TABLE film (
kd_film VARCHAR(10) PRIMARY KEY NOT NULL,
nm_film VARCHAR(55) NOT NULL,
genre VARCHAR(55) NOT NULL,
artis VARCHAR(55) NOT NULL,
produser VARCHAR(55) NOT NULL,
pendapatan INT NOT NULL,
nominasi INT
)

INSERT INTO film (kd_film, nm_film, genre, artis, produser, pendapatan, nominasi)
VALUES
('F001', 'IRON MAN', 'G001', 'A001', 'PD01', 2000000000, 3),
('F002', 'IRON MAN 2', 'G001', 'A001', 'PD01', 1800000000, 2),
('F003', 'IRON MAN 3', 'G001', 'A001', 'PD01', 1200000000, 0),
('F004', 'AVENGER : CIVIL WAR', 'G001', 'A001', 'PD01', 2000000000, 1),
('F005', 'SPIDERMAN HOME COMING', 'G001', 'A001', 'PD01', 1300000000, 0),
('F006', 'THE RAID', 'G001', 'A004', 'PD03', 800000000, 5),
('F007', 'FAST & FURRIOUS', 'G001', 'A004', 'PD05', 830000000, 2),
('F008', 'HABIBIE DAN AINUN', 'G004', 'A005', 'PD03', 670000000, 4),
('F009', 'POLICE STORY', 'G001', 'A003', 'PD02', 700000000, 3),
('F010', 'POLICE STORY 2', 'G001', 'A003', 'PD02', 710000000, 1),
('F011', 'POLICE STORY 3', 'G001', 'A003', 'PD02', 615000000, 0),
('F012', 'RUSH HOUR', 'G003', 'A003', 'PD05', 695000000, 2),
('F013', 'KUNGFU PANDA', 'G003', 'A003', 'PD05', 923000000, 5)

ALTER TABLE film ALTER COLUMN pendapatan INT NOT NULL

SELECT * FROM film

CREATE TABLE produser (
kd_produser VARCHAR(50) PRIMARY KEY NOT NULL,
nm_produser VARCHAR(50) NOT NULL,
international VARCHAR(50) NOT NULL
)

INSERT INTO produser (kd_produser, nm_produser, international)
VALUES
('PD01', 'MARVEL', 'YA'),
('PD02', 'HONGKONG CINEMA', 'YA'),
('PD03', 'RAPI FILM', 'TIDAK'),
('PD04', 'PARKIT', 'TIDAK'),
('PD05', 'PARAMOUNT PICTURE', 'YA')

SELECT * FROM produser

CREATE TABLE negara (
kd_negara VARCHAR(100) PRIMARY KEY NOT NULL,
nm_negara VARCHAR(100) NOT NULL
)

INSERT INTO negara (kd_negara, nm_negara)
VALUES
('AS', 'AMERIKA SERIKAT'),
('HK', 'HONGKONG'),
('ID', 'INDONESIA'),
('IN', 'INDIA')

SELECT * FROM negara

CREATE TABLE genre (
kd_genre VARCHAR(50) PRIMARY KEY NOT NULL,
nm_genre VARCHAR(50)
)

INSERT INTO genre (kd_genre, nm_genre)
VALUES
('G001', 'ACTION'),
('G002', 'HORROR'),
('G003', 'COMEDY'),
('G004', 'DRAMA'),
('G005', 'THRILLER'),
('G006', 'FICTION')

SELECT * FROM genre

-- SOAL NOMOR 1 (Menampilkan jumlah pendapatan produser marvel secara keseluruhan)
SELECT PR.nm_produser, SUM(CAST(FL.pendapatan AS BIGINT)) AS pendapatan
FROM produser AS PR
JOIN film AS FL ON PR.kd_produser = FL.produser
WHERE nm_produser = 'MARVEL'
GROUP BY PR.nm_produser

--SOAL NOMOR 2 (Menampilkan nama film dan nominasi yang tidak mendapatakan nominasi)
SELECT nm_film, nominasi
FROM film
WHERE nominasi = 0

--SOAL NOMOR 3 (Menampilkan nama film yang huruf depannya 'p')
SELECT nm_film
FROM film
WHERE nm_film LIKE 'p%'

--SOAL NOMOR 4 (Menampilkan nama film yang huruf terakhir 'y')
SELECT nm_film
FROM film
WHERE nm_film LIKE '%y'

--SOAL NOMOR 5 (Menampilkan nama film yang mengandung huruf 'd')
SELECT nm_film
FROM film
WHERE nm_film LIKE '%d%'

--SOAL NOMOR 6 (Menampilkan nama film dan artis)
SELECT FL.nm_film, AR.nm_artis
FROM film AS FL
JOIN artis AS AR ON FL.artis = AR.kd_artis

--SOAL NOMOR 7 (Menampilkan nama film yang artisnya berasal dari negara hongkong)
SELECT FL.nm_film, AR.negara
FROM film AS FL
JOIN artis AS AR ON FL.artis = AR.kd_artis
WHERE AR.negara = 'HK'

--SOAL NOMOR 8 (Menampilkan nama film yang artisnya bukan berasal dari negara yang mengandung huruf 'o')
SELECT FL.nm_film, NE.nm_negara
FROM film AS FL
JOIN artis AS AR ON FL.artis = AR.kd_artis
JOIN negara AS NE ON AR.negara = NE.kd_negara
WHERE NE.nm_negara NOT LIKE '%o%'

--SOAL NOMOR 9 (Menampilkan nama artis yang tidak pernah bermain film)
SELECT AR.nm_artis
FROM film AS FL
RIGHT JOIN artis AS AR ON FL.artis = AR.kd_artis
WHERE FL.artis IS NULL

--SOAL NOMOR 10 (Menampilkan nama artis yang bermain film dengan genre drama)
SELECT AR.nm_artis, GE.nm_genre
FROM artis AS AR
JOIN film AS FL ON AR.kd_artis = FL.artis
JOIN genre AS GE ON FL.genre = GE.kd_genre
WHERE GE.nm_genre = 'DRAMA'

--SOAL NOMOR 11 (Menampilkan nama artis yang bermain film dengan genre ACTION)
SELECT DISTINCT(AR.nm_artis), GE.nm_genre
FROM artis AS AR
JOIN film AS FL ON AR.kd_artis = FL.artis
JOIN genre AS GE ON FL.genre = GE.kd_genre
WHERE GE.nm_genre = 'ACTION'

--SOAL NOMOR 12 (Menampilkan data negera dengan jumlah filmnya)
SELECT NG.kd_negara, COUNT(FL.artis) AS jumlah_film
FROM negara AS NG
FULL JOIN artis AS AR ON NG.kd_negara = AR.negara
LEFT JOIN film AS FL ON AR.kd_artis = FL.artis
GROUP BY NG.kd_negara, NG.nm_negara
ORDER BY NG.nm_negara ASC

SELECT NG.kd_negara, COUNT(FL.artis) AS jumlah_film
FROM negara AS NG
LEFT JOIN artis AS AR ON NG.kd_negara = AR.negara
LEFT JOIN film AS FL ON AR.kd_artis = FL.artis
GROUP BY NG.kd_negara

SELECT NG.kd_negara, COUNT(FL.artis) AS jumlah_film
FROM negara AS NG
LEFT JOIN artis AS AR ON NG.kd_negara = AR.negara
FULL JOIN film AS FL ON AR.kd_artis = FL.artis
GROUP BY NG.kd_negara

--SOAL NOMOR 13 (Menampilkan nama film yang skala internasional)
SELECT FL.nm_film
FROM film AS FL
JOIN produser AS PR ON FL.produser = PR.kd_produser
WHERE PR.international = 'YA'

--SOAL NOMOR 14 (Menampilkan jumlah film dari masing2 produser)
SELECT PR.nm_produser, COUNT(FL.nm_film) AS jumlah_film
FROM produser AS PR
LEFT JOIN film AS FL ON PR.kd_produser = FL.produser
GROUP BY PR.nm_produser

SELECT PR.nm_produser, COUNT(FL.nm_film) AS jumlah_film
FROM produser AS PR
FULL JOIN film AS FL ON PR.kd_produser = FL.produser
GROUP BY PR.nm_produser
