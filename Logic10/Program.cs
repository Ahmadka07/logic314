﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Logic10
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //solveMeFirtsHackrank();
            //simpleArraySumHackrank();
            //compareTheTripletsHackrank();
            //aVeryBigSumHackrank();
            //diagonalDifferenceHackrank();
            //plusMinusHackrank();
            //stairCaseHackrank();
            //miniMaxSumHackrank();
            //birthdayCakeCandlesHackrank();
            //timeConversionHackrank();

            Console.ReadKey();
        }

        static void solveMeFirtsHackrank()
        {
            Console.WriteLine("---HackRank Solve Me First---");
            Console.Write("Masukan nilai a = ");
            int c = int.Parse(Console.ReadLine());
            Console.Write("Masukan nilai b = ");
            int d = int.Parse(Console.ReadLine());

            Console.WriteLine(solveMeFirst(c, d));
        }
        static int solveMeFirst(int a, int b)
        { 
            return a + b;
        }

        static void simpleArraySumHackrank()
        {
            Console.WriteLine("---Hackrank Simple Array Sum---");

            List<int> list = new List<int>()
            {
                1, 2, 3, 4, 10, 11
            };

            Console.WriteLine(simpleArraySum(list));
        }
        static int simpleArraySum(List<int> ar)
        {
            int sum = 0;
            for (int i = 0; i < ar.Count; i++)
            {
                sum += ar[i];
            }
            return sum;
        }

        static void compareTheTripletsHackrank()
        {
            Console.WriteLine("---Hackrank Compare The Triplets");

            List<int> a = new List<int>();
            a.Add(5); a.Add(6); a.Add(7);

            List<int> b = new List<int>();
            b.Add(3); b.Add(6); b.Add(10);

            foreach (int item in compareTheTriplets(a, b))
            {
                Console.Write($"{item} ");
            }
        }
        public static List<int> compareTheTriplets(List<int> a, List<int> b)
        {
            int c = 0; int d = 0;
            for (int i = 0; i < a.Count; i++)
            {
                if (a[i] > b[i])
                {
                    c++;
                }
                else if (a[i] < b[i])
                {
                    d++;
                }
            }
            List<int> ar = new List<int>();
            ar.Add(c);
            ar.Add(d);
            return ar;
        }

        static void aVeryBigSumHackrank()
        {
            Console.WriteLine("---Hackrank A Very Big Sum---");

            List<long> ar = new List<long>()
            {
                1000000001, 1000000002, 1000000003, 1000000004, 1000000005
            };

            Console.WriteLine(aVeryBigSum(ar));
        }
        public static long aVeryBigSum(List<long> ar)
        {
            long hasil = 0;

            for (int i = 0; i < ar.Count; i++)
            {
                hasil += ar[i];
            }
            return hasil;
        }

        static void diagonalDifferenceHackrank()
        {
            Console.WriteLine("---Hackrank Diagonal Diffrence---");

            List<List<int>> arr = new List<List<int>>
            {
                new List<int> {11, 2, 4},
                new List<int> {4, 5, 6},
                new List<int> {10, 8, -12}
            };

            Console.WriteLine(diagonalDifference(arr));
        }
        public static int diagonalDifference(List<List<int>> arr)
        {
            int diagonalDifference = 0;
            int diagonal1 = 0;
            int diagonal2 = 0;

            for (int i = 0; i < arr.Count; i++)
            {
                diagonal1 += arr[i][i];
            }

            for (int j = 0; j < arr.Count; j++)
            {
                diagonal2 += arr[j][arr.Count - 1 - j];
            }
            diagonalDifference = diagonal1 - diagonal2;
            return Math.Abs(diagonalDifference);
        }

        static void plusMinusHackrank()
        {
            Console.WriteLine("---Hackrank Plus Minus---");
            
            List<int> arr = new List<int>()
            {
                -4, 3, -9, 0, 4, 1
            };

            double positif = 0;
            double negatif = 0;
            double nol = 0;
            for (int i = 0; i < arr.Count; i++)
            {
                if (arr[i] == 0)
                {
                    nol++;
                }
                else if (arr[i] > 0)
                {
                    positif++;
                }
                else
                {
                    negatif++;
                }
            }
            double hasilPositif = (double)positif / arr.Count;
            double hasilNegatif = (double)negatif / arr.Count;
            double hasilNol = (double)nol / arr.Count;

            Console.WriteLine(Math.Round(hasilPositif, 6));
            Console.WriteLine(Math.Round(hasilNegatif, 6));
            Console.WriteLine(Math.Round(hasilNol, 6));
        }

        static void stairCaseHackrank()
        {
            Console.WriteLine("---Hackrank Staircase---");

            int n = 6;

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (j < n - 1 - i)
                    {
                        Console.Write(" ");
                    }
                    else
                    {
                        Console.Write("#");
                    }
                }
                Console.Write("\n");
            }
        }

        static void miniMaxSumHackrank()
        {
            Console.WriteLine("---Hackrank Mini Max Sum---");

            List<int> arr = new List<int>()
            {
                7, 69, 2, 221, 8974
            };

            /*int tmp = 0; 

            for (int a = 0; a < arr.Count; a++)
            {
                for (int b = 0; b < arr.Count - 1; b++)
                {
                    if (arr[b] > arr[b+1])
                    {
                        tmp = arr[b];
                        arr[b] = arr[b + 1];
                        arr[b + 1] = tmp;
                    }
                }
            }

            int sum1 = 0; int sum2 = 0;
            for (int i = 0; i < arr.Count - 1; i++)
            {
                sum1 += arr[i];
            }
            for (int j = 0; j < arr.Count; j++)
            {
                if (j > 0)
                {
                    sum2 += arr[j];
                }

            }
            Console.Write($"{sum1} ");
            Console.Write(sum2);*/

            List<long> array = new List<long>();
            for (int i = 0; i < arr.Count; i++)
            {
                long temp = 0;
                for (int j = 0; j < arr.Count; j++)
                {
                    if (i != j)
                    {
                        temp += Convert.ToInt64(arr[j]);
                    }
                }
                array.Add(temp);
            }
            long min = 0; long max = 0;
            for (int i = 0; i < array.Count; i++)
            {
                if (array[i] > max)
                {
                    max = array[i];
                }
                if (array[i] < min || min == 0)
                {
                    min = array[i];
                }
            }
            //Console.WriteLine(array.Min().ToString() + " " + array.Max().ToString());
            Console.WriteLine(min.ToString() + " " + max.ToString());
        }

        static void birthdayCakeCandlesHackrank()
        {
            Console.WriteLine("---Hackrank Birthday Cake Candles---");

            List<int> candles = new List<int>()
            {
                3, 2, 1, 3
            };

            Console.WriteLine(birthdayCakeCandles(candles));
        }
        public static int birthdayCakeCandles(List<int> candles)
        {
            int nilaiMax = 0;
            int hitung = 0;

            for (int i = 0; i < candles.Count; i++)
            {
                if (candles[i] >= nilaiMax)
                {
                    nilaiMax = candles[i];
                }

            }

            for (int j = 0; j < candles.Count; j++)
            {
                if (candles[j] == nilaiMax)
                {
                    hitung++;
                }
            }

            return hitung;
        }

        static void timeConversionHackrank()
        {
            Console.WriteLine("---Hackrank Birthday Cake Candles---");

            string s = "07:05:45PM";

            Console.WriteLine(timeConversion(s));
        }
        public static string timeConversion(string s)
        {
            string hasil = "";
            int jam = int.Parse(s.Substring(0, 2));
            string pm = s.Substring(8, 2).ToUpper();

            if (pm == "PM")
            {
                if (jam < 12)
                {
                    jam = jam + 12;
                    hasil = jam.ToString() + s.Substring(2, 6);
                }
                else
                {
                    hasil = s.Substring(0, 8);
                }
            }
            else if (pm == "AM")
            {
                if (jam >= 12)
                {
                    jam = jam - 12;
                    if (jam == 0)
                    {
                        hasil = jam.ToString() + "0" + s.Substring(2, 6);
                    }
                    else
                    {
                        hasil = jam.ToString() + s.Substring(2, 6);
                    }

                }
                else
                {
                    hasil = s.Substring(0, 8);
                }
            }
            return hasil;
        }

    }
}
