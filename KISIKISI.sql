--Kisi-Kisi

--SOAL 01
CREATE DATABASE DB_KISIKISI
USE DB_KISIKISI

CREATE TABLE assigment(
id INT PRIMARY KEY IDENTITY(1,1),
name VARCHAR(50) NOT NULL,
marks VARCHAR(50) NULL,
grade INT NULL
)

ALTER TABLE assigment ALTER COLUMN grade INT NULL

INSERT INTO assigment (name, marks)
VALUES
('Isni', 85),
('Laudry', 75),
('Bambang', 40),
('Anwar', 91),
('Alwi', 70),
('Fulan', 50)

SELECT name, marks, 
CASE
	WHEN marks > 90 THEN 'A+'
	WHEN marks > 70 THEN 'A'
	WHEN marks > 60 THEN 'B'
	WHEN marks > 40 THEN 'C'
	ELSE 'FAIL'
END
AS [grade]
FROM assigment

SELECT * FROM assigment 

INSERT INTO assigment (name, marks)
VALUES 
('Data', 100)

DELETE FROM assigment WHERE id = '7'

--SOAL 02
CREATE TABLE employee(
id INT PRIMARY KEY IDENTITY(1,1),
name VARCHAR(50) NOT NULL,
)

ALTER TABLE employee ADD department_id INT NULL

INSERT INTO employee(name)
VALUES
('Isni'),
('Laudry'),
('Firda'),
('Asti')

CREATE TABLE department (
id INT PRIMARY KEY IDENTITY(1,1),
name VARCHAR(50) NOT NULL,
salary DECIMAL(18,2) NOT NULL
)

ALTER TABLE employee ADD salary DECIMAL(18,2)

ALTER TABLE department DROP COLUMN salary

INSERT INTO department(name)
VALUES
('HRD'),
('Acounting'),
('IT'),
('Finance')

UPDATE employee SET department_id = 1, salary = 100000 WHERE id in(1,2) 

UPDATE employee SET department_id = 1, salary = 50000 WHERE id = 2

UPDATE employee SET department_id = 3, salary = 200000 WHERE id = 3 
UPDATE employee SET department_id = 4, salary = 250000 WHERE id = 4 

SELECT * 
FROM employee emp
JOIN department dep ON emp.id = dep.id

SELECT dep.name AS nama_department, AVG(emp.salary)  
FROM employee emp
JOIN department dep ON emp.department_id = dep.id
GROUP BY dep.name

SELECT dep.name AS nama_department, CAST(AVG(emp.salary) AS decimal(18,2)) AS rata  
FROM employee emp
JOIN department dep ON emp.department_id = dep.id
GROUP BY dep.name
ORDER BY rata DESC


SELECT * FROM employee
SELECT * FROM department
