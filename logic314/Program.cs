﻿using System; //Libary

namespace Logic314 // Tempat untuk menampung banyak class
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");  // Membuat baris baru.
            Console.Write("Hello Guys!");   //Tidak menambah baris baru. 
            Console.WriteLine(" Test"); //Menambah baris baru.

            Console.Write("Masukan nama anda: ");
            String input = Console.ReadLine();
            Console.WriteLine("Nama anda adalah = " + input); //Contoh pemanggilan nama variable string
            Console.WriteLine("Contoh Lain = {0}", input); //Contoh pemanggilan nama variable string
            Console.WriteLine($"Sample Input = {input}"); //Contoh pemanggilan nama variable string

            Console.ReadKey(); //Untuk menahan program agar tidak close.
        }
    }
}
