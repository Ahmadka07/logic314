CREATE DATABASE DBPenerbit
USE DBPenerbit

CREATE TABLE tblPengarang (
ID INT PRIMARY KEY IDENTITY (1,1),
kd_Pengarang VARCHAR (7) NOT NULL,
Nama VARCHAR (30) NOT NULL,
Alamat VARCHAR (80) NOT NULL,
Kota VARCHAR (15) NOT NULL,
Kelamin VARCHAR (1) NOT NULL
)

INSERT INTO tblPengarang (kd_Pengarang, Nama, Alamat, Kota, Kelamin)
VALUES 
('P0001', 'Ashadi', 'Jl. Beo 25', 'Yogya', 'P'),
('P0002', 'Rian', 'Jl. Solo 123', 'Yogya', 'P'),
('P0003', 'Suwadi', 'Jl. Semangka 13', 'Bandung', 'P'),
('P0004', 'Siti', 'Jl. Durian 15', 'Solo', 'W'),
('P0005', 'Amir', 'Jl. Gajah 33', 'Kudus', 'P'),
('P0006', 'Suparman', 'Jl. Harimau 25', 'Jakarta', 'P'),
('P0007', 'Jaja', 'Jl. Singa 7', 'Bandung', 'P'),
('P0008', 'Saman', 'Jl. Naga 12', 'Yogya', 'P'),
('P0009', 'Anwar', 'Jl. Tidar 6A', 'Magelang', 'P'),
('P0010', 'Fatmawati', 'Jl. Renjana', 'Bogor', 'W')

SELECT * FROM tblPengarang

CREATE TABLE tblGaji (
ID INT PRIMARY KEY IDENTITY (1,1),
Kd_Pengarang VARCHAR (7) NOT NULL,
Nama VARCHAR (30) NOT NULL,
Gaji DECIMAL (18,4) NOT NULL
)

INSERT INTO tblGaji (Kd_Pengarang, Nama, Gaji)
VALUES
('P0002', 'Rian', 600000), ('P0005', 'Amir', 700000), 
('P0004', 'Siti', 500000), ('P0003', 'Suwadi', 1000000),
('P0010', 'Fatmawati', 600000), ('P0008', 'Saman', 750000)

SELECT * FROM tblGaji

--Soal Nomor 1
SELECT COUNT (nama) AS [Jumlah Pengarang] FROM tblPengarang
SELECT COUNT (ID) AS Jumlah_Pengarang FROM tblPengarang


-- Soal Nomor 2
SELECT kelamin,
CASE
	WHEN Kelamin = 'W' THEN COUNT (Kelamin)
	WHEN Kelamin = 'P' THEN COUNT (Kelamin)
END AS [Jumlah Pengarang]
FROM tblPengarang
GROUP BY Kelamin

SELECT Kelamin, COUNT(Kelamin) AS Jumlah FROM tblPengarang WHERE Kelamin = 'P' OR Kelamin = 'W' GROUP BY Kelamin 

SELECT Kelamin, COUNT(Kelamin) AS Jumlah_Kelamin FROM tblPengarang GROUP BY Kelamin

--Soal Nomor 3
SELECT Kota, COUNT (Kota) AS [Jumlah Kota]
FROM tblPengarang
GROUP BY Kota

--Soal Nomor 4
SELECT Kota, COUNT (Kota) AS [Jumlah Kota]
FROM tblPengarang
GROUP BY Kota
HAVING COUNT (Kota) > 1

--Soal Nomor 5
SELECT MAX(Kd_Pengarang) AS [Kode Pengarang Terbesar], MIN(Kd_Pengarang) AS [Kode Pengarang Terkecil] 
FROM tblPengarang

SELECT
(SELECT TOP 1 Kd_Pengarang FROM tblPengarang ORDER BY Kd_Pengarang DESC) AS terbesar,
(SELECT TOP 1 Kd_Pengarang FROM tblPengarang ORDER BY Kd_PEngarang ASC) AS terkecil

--Soal Nomor 6
SELECT MAX(Gaji) AS [Gaji Terbesar], MIN(Gaji) AS [Gaji Terkecil]
FROM tblGaji

--Soal Nomor 7
SELECT Gaji AS [Gaji lebih dari 600000]
FROM tblGaji
GROUP BY Gaji
HAVING Gaji > 600000

SELECT Gaji FROM tblGaji WHERE Gaji > 600000

--Soal Nomor 8
SELECT SUM(Gaji) AS [Jumlah Gaji]
FROM tblGaji

--Soal Nomor 9
SELECT tblp.Kota, SUM(tblgj.Gaji) AS [Jumlah Gaji] 
FROM tblGaji tblgj
JOIN tblPengarang tblp ON tblgj.Kd_Pengarang = tblp.kd_Pengarang
GROUP BY tblp.Kota

SELECT kota, sum(Gaji) AS Jumlah_Gaji
FROM tblPengarang AS pengarang
LEFT JOIN tblGaji AS Gaji
ON pengarang.Kd_Pengarang = Gaji.Kd_Pengarang
GROUP BY Kota

--Soal Nomor 10
SELECT Kd_Pengarang
FROM tblPengarang
WHERE kd_Pengarang BETWEEN 'P0003' AND 'P0006'

SELECT * FROM tblPengarang
WHERE Kd_Pengarang >= 'P0002' AND Kd_Pengarang <= 'P0005'

--Soal Nomor 11
SELECT * 
FROM tblPengarang
WHERE Kota = 'Yogya' OR Kota = 'Solo' OR Kota = 'Magelang'

SELECT * FROM tblPengarang WHERE Kota IN ('Yogya', 'Solo', 'Magelang')

--Soal Nomor 12
Select *
FROM tblPengarang
WHERE Kota != 'Yogya'

Select * FROM tblPengarang WHERE Kota <> 'Yogya'

SELECT * FROM tblPengarang WHERE NOT Kota = 'Yogya'

--Soal Nomor 13 a
SELECT * 
FROM tblPengarang
WHERE Nama LIKE 'A%'

--Soal Nomor 13 b
SELECT * 
FROM tblPengarang
WHERE Nama LIKE '%i'

SELECT * FROM tblPengarang WHERE Kd_Pengarang NOT LIKE '%4' AND Nama LIKE '%i'
SELECT * FROM tblPengarang WHERE Kd_Pengarang NOT LIKE '%___4' AND Nama LIKE '%i'

--Soal Nomor 13 c
SELECT * 
FROM tblPengarang
WHERE Nama LIKE '__a%'

--Soal Nomor 13 d
SELECT * 
FROM tblPengarang
WHERE Nama NOT LIKE '%n'

--Soal Nomor 14
SELECT tblp.Kd_Pengarang, tblp.ID, tblp.Nama, tblp.Kota, tblp.Alamat, tblp.Kelamin, tblgj.Gaji
FROM tblPengarang AS tblp
JOIN tblGaji AS tblgj ON tblp.kd_Pengarang = tblgj.Kd_Pengarang

SELECT * FROM tblPengarang AS pg
JOIN tblGaji AS gj ON pg.Kd_Pengarang = gj.Kd_Pengarang

--Soal Nomor 15
SELECT tblp.Kd_Pengarang, tblp.ID, tblp.Nama, tblp.Kota, tblp.Alamat, tblp.Kelamin, tblgj.Gaji
FROM tblPengarang AS tblp
JOIN tblGaji AS tblgj ON tblp.kd_Pengarang = tblgj.Kd_Pengarang
WHERE Gaji < 1000000

SELECT tblPengarang.Kota, tblGaji.Gaji FROM tblGaji
JOIN tblPengarang ON tblGaji.Kd_Pengarang = tblPengarang.Kd_Pengarang
WHERE tblGaji.Gaji < 1000000

--Soal Nomor 16
ALTER TABLE tblPengarang ALTER COLUMN Kelamin VARCHAR (10) NOT NULL

--Soal Nomor 17
ALTER TABLE tblPengarang ADD [Gelar] VARCHAR (12)

--Soal Nomor 18
UPDATE tblPengarang SET
Alamat = 'Jl. Cendrawasih 65',
Kota = 'Pekanbaru'
WHERE Nama = 'Rian'

UPDATE tblPengarang SET
Alamat = 'Jl. Solo 123',
Kota = 'Yogya'
WHERE Nama = 'Rian'

SELECT * FROM tblPengarang

--Soal Nomor 19
CREATE VIEW vwPengarang
AS 
SELECT *
FROM tblPengarang AS tblp
FULL JOIN tblGaji AS tblgj ON tblp.kd_Pengarang = tblgj.Kd_Pengarang

DROP VIEW vwPengarang

SELECT * FROM vwPengarang

CREATE VIEW vwPengarang1
AS
SELECT tblp.Kd_Pengarang, tblp.Nama, tblp.Kota, tblgj.Gaji
FROM tblPengarang tblp, tblGaji tblgj
WHERE tblp.kd_Pengarang = tblgj.Kd_Pengarang

SELECT * FROM vwPengarang

SELECT * FROM vwPengarang1
