CREATE DATABASE DB_PTXA
USE DB_PTXA

CREATE TABLE Biodata(
id BIGINT PRIMARY KEY IDENTITY(1,1) NOT NULL,
first_name VARCHAR(20),
last_name VARCHAR(30),
dob DATETIME,
pob VARCHAR(50),
address VARCHAR(255),
gender VARCHAR(1)
)

INSERT INTO Biodata (first_name, last_name, dob, pob, address, gender)
VALUES
('soraya', 'rahayu', '1990-12-22', 'Bali', 'Jl. Raya Kuta, Bali', 'P'),
('hanum', 'danuary', '1990-01-02', 'Bandung', 'Jl. Berkah Ramadhan, Bandung', 'P'),
('melati', 'marcelia', '1991-03-03', 'Jakarta', 'Jl. Mawar 3, Brebes', 'P'),
('farhan', 'Djokrowidodo', '1989-10-11', 'Jakarta', 'Jl. Bahari Raya, Solo', 'L')

SELECT * FROM Biodata

CREATE TABLE Employee(
id BIGINT PRIMARY KEY IDENTITY(1,1) NOT NULL,
biodata_id BIGINT,
nip VARCHAR(5),
status VARCHAR(10),
join_date DATETIME,
salary decimal(10,0)
)

INSERT INTO Employee(biodata_id, nip, status, join_date, salary)
VALUES
( 1, 'XA001', 'Permanen', '2015-11-01', 12000000),
( 2, 'XA002', 'Kontrak', '2017-01-02', 10000000),
( 3, 'XA003', 'Kontrak', '2018-08-19', 10000000)

SELECT * FROM Employee

CREATE TABLE Contact_Person(
id BIGINT,
biodata_id BIGINT,
type VARCHAR(5),
contact VARCHAR(100)
)

INSERT INTO Contact_Person(id, biodata_id, type, contact)
VALUES
( 1, 1, 'MAIL', 'soraya.rahayu@gmail.com'),
( 2, 1, 'PHONE', '085612345678'),
( 3, 2, 'MAIL', 'hanum.danuary@gmail.com'),
( 4, 2, 'PHONE', '081312345678'),
( 5, 2, 'PHONE', '087812345678'),
( 6, 3, 'MAIL', 'melati.marcelia@gmail.com')

SELECT * FROM Contact_Person

CREATE TABLE Leave(
id BIGINT,
type VARCHAR(10),
name VARCHAR(100)
)

INSERT INTO Leave (id, type, name)
VALUES
( 1, 'Reguler', 'Cuti Tahunan'),
( 2, 'Khusus', 'Cuti Menikah'),
( 3, 'Khusus', 'Cuti Haji & Umroh'),
( 4, 'Khusus', 'Melahirkan')

SELECT * FROM Leave

CREATE TABLE EmployeeLeave(
id INT,
employee_id INT,
period VARCHAR(4),
regular_quota INT
)

INSERT INTO EmployeeLeave(id, employee_id, period, regular_quota)
VALUES
( 1, 1, '2021', 16),
( 2, 2, '2021', 12),
( 3, 3, '2021', 12)

SELECT * FROM EmployeeLeave

CREATE TABLE LeaveRequest(
id BIGINT,
employee_id BIGINT,
leave_id BIGINT,
start_date DATE,
end_date DATE,
reason VARCHAR(255)
)

INSERT INTO LeaveRequest(id, employee_id, leave_id, start_date, end_date, reason)
VALUES
( 1, 1, 1, '2021-10-10', '2021-10-12', 'Liburan'),
( 2, 1, 1, '2021-11-12', '2021-11-15', 'Acara Keluarga'),
( 3, 2, 2, '2021-05-05', '2021-05-07', 'Menikah'),
( 4, 2, 1, '2021-09-09', '2021-09-13', 'Touring'),
( 5, 2, 1, '2021-12-20', '2021-12-23', 'Acara Keluarga')

SELECT * FROM LeaveRequest

--SOAL NOMOR 1 (Menampilkan karyawan yang pertama kali masuk)
SELECT TOP 1 CONCAT(BD.first_name, ' ', BD.last_name) AS nama, CAST(EM.join_date AS DATE) AS tanggal_masuk
FROM Biodata AS BD
JOIN Employee AS EM ON BD.id = EM.biodata_id
ORDER BY EM.join_date ASC

SELECT CONCAT(BD.first_name, ' ', BD.last_name) AS nama, CAST(Employee.join_date AS DATE) AS tanggal_masuk
FROM Biodata AS BD
JOIN Employee ON BD.id = Employee.biodata_id
WHERE Employee.join_date = (SELECT(MIN(join_date)) FROM Employee)
ORDER BY Employee.join_date ASC

--SOAL NOMOR 2 (Menampilkan daftar karyawan yang saat ini sedang cuti. Daftar berisi nomor induk, nama, tanggal mulai, lama cuti dan keterangan)
SELECT EM.nip, CONCAT(BD.first_name, ' ', BD.last_name) AS nama, LR.start_date AS tanggal_mulai,
DATEDIFF(DAY, LR.start_date, LR.end_date) AS lama_Cuti, LR.reason AS keterangan
FROM Biodata AS BD
JOIN Employee AS EM ON BD.id = EM.biodata_id
JOIN LeaveRequest AS LR ON EM.biodata_id = LR.employee_id
WHERE LR.end_date > '2021-12-22'

--SOAL NOMOR 3 (Menampilkan daftar karyawan yang sudah mengajukan cuti lebih dari 2 kali. Tampilkan data berisi no induk, nama, jumlah pengajuan)
SELECT EM.nip, CONCAT(BD.first_name, ' ', BD.last_name) AS nama, COUNT(LR.employee_id) AS Jumlah_Pengajuan
FROM Biodata AS BD
JOIN Employee AS EM ON BD.id = EM.biodata_id
JOIN LeaveRequest AS LR ON EM.biodata_id = LR.employee_id
GROUP BY EM.nip, BD.first_name, BD.last_name, LR.employee_id
HAVING COUNT(LR.employee_id) > 2

--SOAL NOMOR 4 (Menampilkan sisa cuti karyawan tahun ini, jika diketahui jatah cuti setiap karyawan tahun ini 
--				adalah sesuai dengan quota cuti. tampilan berisi no induk, nama, quota, cuti yang sudah diambil dan sisa 
--				cuti)
SELECT DISTINCT EM.nip, CONCAT(BD.first_name, ' ', BD.last_name) AS nama, EML.regular_quota,
ISNULL(SUM(DATEDIFF(DAY, LR.start_date, LR.end_date)), 0) AS lama_Cuti, EML.regular_quota - ISNULL(SUM(DATEDIFF(DAY, LR.start_date, LR.end_date)), 0) AS sisa_cuti
FROM Biodata AS BD
JOIN EmployeeLeave AS EML ON BD.id = EML.employee_id
JOIN Employee AS EM ON EML.employee_id = EM.biodata_id
LEFT JOIN LeaveRequest AS LR ON EM.biodata_id = LR.employee_id
GROUP BY EM.nip, BD.first_name, BD.last_name, EML.regular_quota

--SOAL NOMOR 5 (Perusahaan akan memberikan bonus bagi karyawan yang sudah bekerja lebih dari 5 tahun sebanyak 1.5 gaji.
--				Tampilan no induk, full name, berapa lama bekerja, bonus, total gaji(Gaji + Bonus))
SELECT EM.nip, CONCAT(BD.first_name, ' ', BD.last_name) AS nama,
DATEDIFF(YEAR, EM.join_date ,'2021-12-22') AS lama_bekerja, EM.salary * 1.5 AS bonus, EM.salary + (EM.salary * 1.5) AS total_gaji 
FROM Biodata AS BD
JOIN Employee AS EM ON BD.id = EM.biodata_id
WHERE DATEDIFF(YEAR, EM.join_date ,'2021-12-22') > 5

--SOAL NOMOR 6 (Tampilkan nip, nama lengkap, jika karyawan ada yang berulang tahun di hari ini akan diberikan hadiah
--				bonus sebanyak 5% dari gaji jika tidak ulang tahun makan bonus 0 dan total gaji, Tampilkan no induk,
--				tanggal lahir, usia, bonus, total gaji)
SELECT EM.nip, CONCAT(BD.first_name, ' ', BD.last_name) AS nama, CAST(BD.dob AS DATE) AS tanggal_lahir, 
DATEDIFF(YEAR, BD.dob, '2022-12-22') AS usia,
CASE
	WHEN BD.dob = '2022-12-22' THEN EM.salary * 0.05
	ELSE 0
END AS bonus, EM.salary AS total_gaji 
FROM Biodata AS BD
JOIN Employee AS EM ON BD.id = EM.biodata_id

--SOAL NOMOR 7 (Tampilkan no induk, nama, tanggal lahir, usia. Urutkan biodata dari yang paling tua)
SELECT EM.nip, CONCAT(BD.first_name, ' ', BD.last_name) AS nama, CAST(BD.dob AS DATE) AS tanggal_lahir, 
DATEDIFF(YEAR, BD.dob, '2022-12-22') AS usia
FROM Biodata AS BD
JOIN Employee AS EM ON BD.id = EM.biodata_id
ORDER BY usia DESC

--SOAL NOMOR 8 (Tampilkan karyawan yang belum pernah cuti)
SELECT EM.nip, CONCAT(BD.first_name, ' ', BD.last_name) AS nama
FROM Biodata AS BD
JOIN EmployeeLeave AS EML ON BD.id = EML.employee_id
JOIN Employee AS EM ON EML.employee_id = EM.biodata_id
LEFT JOIN LeaveRequest AS LR ON EM.biodata_id = LR.employee_id
GROUP BY EM.nip, BD.first_name, BD.last_name
HAVING ISNULL(SUM(DATEDIFF(DAY, LR.start_date, LR.end_date)), 0) = 0

SELECT EM.nip, CONCAT(BD.first_name, ' ', BD.last_name) AS nama
FROM Biodata AS BD
JOIN EmployeeLeave AS EML ON BD.id = EML.employee_id
JOIN Employee AS EM ON EML.employee_id = EM.biodata_id
LEFT JOIN LeaveRequest AS LR ON EM.biodata_id = LR.employee_id
WHERE LR.employee_id IS NULL

--SOAL NOMOR 9 (Tampilkan nama lengkap, Jenis Cuti, Durasi Cuti, dan No.Telp yang sudah cuti)
SELECT CONCAT(BD.first_name, ' ', BD.last_name) AS nama, LV.type, CP.contact,
ISNULL(SUM(DATEDIFF(DAY, LR.start_date, LR.end_date)), 0) AS lama_Cuti
FROM Biodata AS BD
JOIN EmployeeLeave AS EML ON BD.id = EML.employee_id
JOIN Employee AS EM ON EML.employee_id = EM.biodata_id
LEFT JOIN LeaveRequest AS LR ON EM.biodata_id = LR.employee_id
JOIN Leave AS LV ON LR.leave_id = LV.id
JOIN Contact_Person AS CP ON BD.id = CP.biodata_id
WHERE CP.type = 'PHONE' AND CP.contact NOT LIKE '0813%' 
GROUP BY EM.nip, BD.first_name, BD.last_name, LV.type, CP.contact

--SOAL NOMOR 10 (Tampilkan nama-nama pelamar yang tidak diterima sebagai karyawan)
SELECT CONCAT(BD.first_name, ' ', BD.last_name) AS Nama
FROM Biodata AS BD
LEFT JOIN Employee AS EM ON BD.id = EM.biodata_id
WHERE EM.biodata_id IS NULL

--SOAL NOMOR 11 (Buatlah sebuah view yang menampilkan data nama lengkap, tgl lahir, tempat lahir, status dan salary)
CREATE VIEW vw11
AS
SELECT CONCAT(BD.first_name, ' ', BD.last_name) AS Nama_Lengkap, CAST(BD.dob AS DATE) AS tgl_lahir, BD.pob, EM.status, em.salary
FROM Biodata AS BD
JOIN Employee AS EM ON BD.id = EM.biodata_id

SELECT * FROM vw11 

--SOAL NOMOR 12 (Tampilkan alasan Cuti yang paling sering diajukan)
SELECT reason, COUNT(reason) AS jml_paling_banyak_diajukan
FROM LeaveRequest
GROUP BY reason

SELECT TOP 1 reason
FROM LeaveRequest
GROUP BY reason

