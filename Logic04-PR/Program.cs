﻿using System;

namespace Logic04_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soal1();
            //soal2();
            //soal3();
            //soal4();
            //soal5();
            //soal6();
            //soal7();
            //soal8();
            //soal9();
            //soal10();
            //soal11();
            //soal12();
            //soal12Ver2();
            //soal12Ver3();

            Console.ReadKey();
        }

        static void soal1()
        {
            Console.WriteLine("---Soal Nomor 1---");

            double gol1 = 2000;
            double gol2 = 3000;
            double gol3 = 4000;
            double gol4 = 5000;
            double jamKerjaPerMinggu = 40;
            double upahLembur = 1.5;
            double upah = 0, lembur = 0, total = 0;

            Console.Write("Golongan (1, 2, 3, 4) : ");
            int tipeGol = int.Parse(Console.ReadLine());
            Console.Write("Jam Kerja : ");
            int jamKerja = int.Parse(Console.ReadLine());

            if (tipeGol > 0 && tipeGol <= 4 && jamKerja > 0)
            {
                if (tipeGol == 1)
                {
                    if (jamKerja >= 40)
                    {
                        upah = jamKerjaPerMinggu * gol1;
                        lembur = (jamKerja - jamKerjaPerMinggu) * (gol1 * upahLembur);
                    }
                    else
                    {
                        upah = jamKerja * gol1;

                    }

                }
                else if (tipeGol == 2)
                {
                    if (jamKerja >= 40)
                    {
                        upah = jamKerjaPerMinggu * gol2;
                        lembur = (jamKerja - jamKerjaPerMinggu) * (gol2 * upahLembur);
                    }
                    else
                    {
                        upah = jamKerja * gol2;
                    }

                }
                else if (tipeGol == 3)
                {
                    if (jamKerja >= 40)
                    {
                        upah = jamKerjaPerMinggu * gol3;
                        lembur = (jamKerja - jamKerjaPerMinggu) * (gol3 * upahLembur);
                    }
                    else
                    {
                        upah = jamKerja * gol3;
                    }

                }
                else if (tipeGol == 4)
                {
                    if (jamKerja >= 40)
                    {
                        upah = jamKerjaPerMinggu * gol4;
                        lembur = (jamKerja - jamKerjaPerMinggu) * (gol4 * upahLembur);
                    }
                    else
                    {
                        upah = jamKerja * gol4;
                    }

                }

                total = upah + lembur;
                Console.WriteLine($"Upah : {upah}"); ;
                Console.WriteLine($"Lembur : {lembur}");
                Console.WriteLine($"Total : {total}");
            }
            else
            {
                Console.WriteLine("Kesalahan Input Data");
            }
        }

        static void soal2()
        {
            Console.WriteLine("---Soal Nomor 2---");
            Console.Write("Masukan Kalimat = ");
            string kalimat = Console.ReadLine();
            string[] kataArray = kalimat.Split(" ");
            int nomor = 1;

            foreach (string kata in kataArray)
            {
                Console.WriteLine($"Kata {nomor} = {kata}");
                nomor++;
            }
            Console.WriteLine($"Total kata adalah = {kataArray.Length}");
        }

        static void soal3()
        {
            Console.WriteLine("---Soal Nomor 3---");

            Console.Write("Masukan Kalimat = ");
            string kalimat = Console.ReadLine();
            string[] perKata = kalimat.Split(" ");


            for (int i = 0; i < perKata.Length; i++)
            {
                //Console.WriteLine(perKata[i]);
                for (int j = 0; j < perKata[i].Length; j++)
                {
                    //Console.WriteLine(perKata[i][j]);
                    if (j == 0)
                    {
                        Console.Write(perKata[i][j]);
                    }
                    else if (j == perKata[i].Length - 1)
                    {
                        
                        Console.Write(perKata[i][j]);
                    }
                    else
                    {
                        Console.Write("*");
                    }
                }
                Console.Write(" ");
            }
        }

        static void soal4()
        {
            Console.WriteLine("---Soal Nomor 4---");

            Console.Write("Masukan Kalimat = ");
            string kalimat = Console.ReadLine();
            string[] perKata = kalimat.Split(" ");

            for (int i = 0; i < perKata.Length; i++)
            {
                for (int j = 0; j < perKata[i].Length; j++)
                {
                    if (j == 0)
                    {
                        Console.Write("*");
                    }
                    else if (j == perKata[i].Length - 1)
                    {
                        Console.Write("*");
                    }
                    else
                    {
                        Console.Write(perKata[i][j]);
                    }
                }
                Console.Write(" ");
            }
        }

        static void soal5()
        {
            Console.WriteLine("---Soal Nomor 5---");

            Console.Write("Masukan Kalimat = ");
            //string kalimat = Console.ReadLine();
            string[] perKata = Console.ReadLine().Split(" ");

            for (int i = 0; i < perKata.Length; i++)
            {
                for (int j = 0; j < perKata[i].Length; j++)
                {
                    if (j == 0)
                    {
                        Console.Write(perKata[i].Remove(j));
                    }
                    else
                    {
                        Console.Write(perKata[i][j]);
                    }
                }
                Console.Write(" ");
            }

            //for (int i = 0; i < perKata.Length; i++)
            //{
            //    for (int j = 0; j < perKata[i].Length; j++)
            //    {
            //        if (j == 0)
            //        {
            //            Console.Write("");
            //        }

            //        else
            //        {
            //            Console.Write(perKata[i][j]);
            //        }
            //    }
            //    Console.Write(" ");
            //}

            //for (int i = 0; i < perKata.Length; i++)
            //{
            //    Console.Write(perKata[i].Remove(0, 1));

            //    Console.Write(" ");
            //}
        }

        static void soal6()
        {
            Console.WriteLine("---Soal Nomor 6---");
            Console.Write("Masukan Jumlah Deret = ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("Masukan angka deret = ");
            int deret = int.Parse(Console.ReadLine());
            int[] array = new int[n];
            int hasil = 1;

            for (int i = 0; i < array.Length; i++)
            {
                hasil *= deret;
                array[i] = hasil;
                
                if (i % 2 == 0)
                {
                    Console.Write($"{array[i]} ");
                }
                else
                {
                    Console.Write("* ");
                }
            }
        }

        static void soal7()
        {
            Console.WriteLine("---Soal Nomor 7---");
            Console.Write("Masukan panjang deret = ");
            int n = int.Parse(Console.ReadLine());
            Console.Write("Masukan angka deret = ");
            int deret = int.Parse(Console.ReadLine());
            int[] array = new int[n];

            for (int i = 0; i < array.Length; i++)
            {
                int nilai = i % 2;
                if (nilai == 0)
                {
                    int total = -deret * (i + 1);
                    array[i] = total;
                    Console.Write($"{array[i]} ");
                }
                else
                {
                    int total = deret * (i + 1);
                    array[i] = total;
                    Console.Write($"{array[i]} ");
                }
            }
        }

        static void soal8()
        {
            Console.WriteLine("---Soal Nomor 8---");
            Console.Write("Masukan panjang fibonaci = ");
            int number = int.Parse(Console.ReadLine());

            int[] numberArray = new int[number];

            for (int i = 0; i < number; ++i)
            {
                if (i <= 1)
                {
                    numberArray[i] = 1;
                }
                else
                {
                    numberArray[i] = numberArray[i - 2] + numberArray[i - 1];
                }
            }

            Console.Write(string.Join(" ", numberArray));

            //Console.WriteLine("====== FIBONACCI  =====");
            //Console.Write("Masukkan Jumlah Angka Fibonacci : ");
            //int number = int.Parse(Console.ReadLine());

            //Console.WriteLine();

            //int n1 = 0, n2 = 1, n3;

            //for (int i = 1; i <= number; i++)
            //{
            //    n3 = n1 + n2;
            //    n1 = n3;
            //    n2 = n1;
            //    Console.Write(n3 + ", ");
            //}

            //int n = int.Parse(Console.ReadLine());
            //int a = 0;
            //int b = 1;

            //for (int i = 0; i < n; i++)
            //{
            //    int sum = a + b; // {0}sum = 0 + 1(1), {1}sum = 1 + 1(2), 
            //    a = b; // {0}1, {1}1
            //    b = sum; // {0}1, {1}2
            //    Console.Write($"{a} "); //1, 1
            //}
        }

        static void soal9()
        {
            Console.WriteLine("---Soal Nomor 9---");

            Console.Write("Masukan Format Jam(AM/PM) = ");
            string formatJam = Console.ReadLine().ToUpper();

            string pm = formatJam.Substring(8, 2).ToUpper();
            int jam = int.Parse(formatJam.Substring(0, 2));

            if (pm == "PM")
            {
                if (jam < 12)
                {
                    jam = jam + 12;
                    Console.WriteLine(jam.ToString() + formatJam.Substring(2, 6));
                }
                else
                {
                    Console.WriteLine(formatJam.Substring(0, 8));
                }
            }
            else if (pm == "AM")
            {
                if (jam >= 12)
                {
                    jam = jam - 12;
                    if (jam == 0)
                    {
                        Console.WriteLine(jam.ToString() + "0" + formatJam.Substring(2, 6));
                    }
                    else
                    {
                        Console.WriteLine(jam.ToString() + formatJam.Substring(2, 6));
                    }
                }
                else
                {
                    Console.WriteLine(formatJam.Substring(0, 8));
                }
            }
            else
            {
                Console.WriteLine("Masukan input yang benar");
            }
        }

        static void soal10()
        {
            Console.WriteLine("---Soal Nomor 10---");

            int kodeBaju, harga = 0;
            string kodeUkuran;
            string merekBaju = "";

            Console.Write("Masukan Kode Baju (1, 2, 3) = ");
            kodeBaju = int.Parse(Console.ReadLine());

            baju:

            Console.Write("Masukan Kode Ukuran (S, M, L, XL) = ");
            kodeUkuran = Console.ReadLine().ToUpper();

            if (kodeBaju > 0 && kodeBaju <= 3)
            {
                if (kodeBaju == 1)
                {
                    merekBaju = "IMP";
                    if (kodeUkuran == "S")
                    {
                        harga = 200000;
                    }
                    else if (kodeUkuran == "M")
                    {
                        harga = 220000;
                    }
                    else if (kodeUkuran == "L" || kodeUkuran == "XL")
                    {
                        harga = 250000;
                    }
                    else
                    {
                        Console.WriteLine("Kode ukuran tidak ditemukan, Masukan kembali kode ukuran");
                        goto baju;
                    }
                }
                else if (kodeBaju == 2)
                {
                    merekBaju = "Prada";
                    if (kodeUkuran == "S")
                    {
                        harga = 150000;
                    }
                    else if (kodeUkuran == "M")
                    {
                        harga = 160000;
                    }
                    else if (kodeUkuran == "L" || kodeUkuran == "XL")
                    {
                        harga = 170000;
                    }
                    else
                    {
                        Console.WriteLine("Kode ukuran tidak ditemukan, Masukan kembali kode ukuran");
                        goto baju;
                    }
                }
                else if (kodeBaju == 3)
                {
                    merekBaju = "Gucci";
                    if (kodeUkuran == "S" || kodeUkuran == "M" || kodeUkuran == "L" || kodeUkuran == "XL")
                    {
                        harga = 200000;
                    }
                    else
                    {
                        Console.WriteLine("Kode ukuran tidak ditemukan, Masukan kembali kode ukuran");
                        goto baju;
                    }
                }
                Console.WriteLine($"Merek Baju = {merekBaju}");
                Console.WriteLine($"harga = {harga}");
            }
            else
            {
                Console.WriteLine("Kode Baju Tidak Ada");
                
            }
        }

        static void soal11()
        {
            Console.WriteLine("---Soal Nomor 11---");

            Console.Write("Masukan Uang Andi = ");
            int uang = int.Parse(Console.ReadLine());
            Console.Write("Masukan Harga Baju(pakai koma) = ");
            string[] hargaBajuStrg = Console.ReadLine().Split(",");
            int[] hargaBajuInt = Array.ConvertAll(hargaBajuStrg, int.Parse);
            //int[] hargaBajuArray = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            Console.Write("Masukan Harga Celana(pakai koma) = ");
            int[] hargaCelanaArray = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            int maxBelanja = 0;
            for (int i = 0; i < hargaBajuInt.Length; i++)
            {
                for (int j = 0; j < hargaCelanaArray.Length; j++)
                {
                    int harga = hargaBajuInt[i] + hargaCelanaArray[j];
                    if (harga <= uang && harga >= maxBelanja)
                    {
                        maxBelanja = harga;
                    }
                }
            }
            Console.WriteLine($"Belanja sesuai kebutuhan {maxBelanja}");
        }

        static void soal12()
        {
            Console.WriteLine("---Soal Nomor 12---");

            Console.Write("Masukan Inputan(n) = ");
            int n = int.Parse(Console.ReadLine());

            string tampung = "";

            if (n > 0)
            {
                int[] array = new int[n];

                for (int i = 0; i < array.Length; i++)
                {
                    array[i] = i + 1;
                }

                for (int j = 0; j < array.Length; j++)
                {
                    for (int k = 0; k < array.Length; k++)
                    {
                        if (j == 0)
                        {
                            tampung += array[k] + "\t";
                            //Console.Write($"{array[k]} ");
                        }
                        else if (j == array.Length - 1)
                        {
                            tampung += array[(array.Length - 1) - k] + "\t";
                            //Console.Write($"{array[(array.Length - 1) - k]} ");
                        }
                        else
                        {
                            if (k == 0)
                            {
                                tampung += "*\t";
                                //Console.Write("* ");
                            }
                            else if (k == array.Length - 1)
                            {
                                //Console.Write("* ");
                                tampung += "*\t";
                            }
                            else
                            {
                                tampung += "\t";
                                //Console.Write("  ");
                            }
                        }
                    }
                    tampung += "\n";
                }
                Console.Write(tampung);
            }
            else
            {
                Console.WriteLine("Invalid");
            }
        }

        static void soal12Ver2()
        {
            Console.WriteLine("---Soal Nomor 12---");

            Console.Write("Masukan Inputan(angka) = ");
            int n = int.Parse(Console.ReadLine());

            int deret1 = 1;
            int deret2 = n;

            string tampung = "";

            for (int i = 0; i < n; i++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (i == 0)
                    {
                        tampung += deret1++ + "\t";
                    }
                    else if (i == n - 1)
                    {
                        tampung += deret2-- + "\t";
                    }
                    else if (j == 0 || j == n - 1)
                    {
                        tampung += "*\t";
                    }
                    else
                    {
                        tampung += " \t";
                    }
                }
                tampung += "\n";
            }
            Console.Write(tampung);
        }

        static void soal12Ver3()
        {
            Console.WriteLine("---Soal Nomor 12---");

            Console.Write("Masukan Inputan(angka) = ");
            int n = int.Parse(Console.ReadLine());

            for (int i = 1; i <= n; i++)
            {
                for (int j = 1; j <= n; j++)
                {
                    if (i == 1 || i == n)
                    {
                        if (i == n)
                        {
                            Console.Write(n - j + 1 + "\t");
                        }
                        else
                        {
                            Console.Write(j + "\t");
                        }
                    }
                    else
                    {
                        if (j == 1 || j == n)
                            Console.Write("*\t");
                        else
                            Console.Write(" \t");
                    }
                }
                Console.WriteLine();
            }
        }

    }
}
