﻿using System;
using System.Security.Cryptography;

namespace Logic02
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //ifStatement();
            //elseStatement();
            //ifNested();
            //impFashion();
            //ternary();
            //switchCase();
            //tugas01();

            Console.ReadKey();
        }

        static void tugas01()
        {
            Console.WriteLine("---Keliling dan Luas Lingkaran");
            double r; 
            double keliling, luas;
            double phi = 3.14;

            Console.Write("Masukan jari-jari lingkaran = ");
            r = Convert.ToDouble(Console.ReadLine());


            keliling = 2 * phi * r;
            Console.WriteLine($"Keliling Lingkaran = {keliling}");

            luas = phi * r * r;
            Console.WriteLine($"Luas Lingkaran = {luas}");
            
            Console.WriteLine();
            Console.WriteLine();

            Console.WriteLine("---Luas dan Keliling Persegi---");
            int s, kelPersegi, luasPersegi;

            Console.Write("Masukan sisi persegi = ");
            s = int.Parse(Console.ReadLine());

            kelPersegi = 4 * s;
            Console.WriteLine($"Keliling Persegi = {kelPersegi}");

            luasPersegi = s * s;
            Console.WriteLine($"Luas Persegi = {luasPersegi}");

        }

        static void switchCase() {
            Console.WriteLine("---Switch Case---");
            Console.WriteLine("Pilih buah kesukaan kalian (apel, mangga, pisang) ? ");
            string input = Console.ReadLine().ToLower();

            switch (input) {

                case "apel":
                    Console.WriteLine("Anda memilih buah Apel");
                    break;
                case "mangga":
                    Console.WriteLine("Anda memilih buah Mangga");
                    break;
                case "pisang":
                    Console.WriteLine("Anda memilih buah Pisang");
                    break;
                default:
                    Console.WriteLine("Anda memilih buah yang lain");
                    break;
            }
        }

        static void ternary() {

            Console.WriteLine("---Ternary---");
            int x, y;
            Console.Write("Masukan Nilai x = ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukan Nilai y = ");
            y = int.Parse(Console.ReadLine());

            string hasilTernary = x > y ? "Nilai x lebih besar dari nilai y" :
                x < y ? "Nilai x lebih kecil dari nilai y" : "Nilai x sama dengan nilai y";

            Console.WriteLine(hasilTernary);
        }

        static void ifNested() {
            Console.WriteLine("---If Nested---");
            Console.Write("Masukan Nilai = ");
            int nilai = int.Parse(Console.ReadLine());
            int maxNilai = 100;

            if (nilai >= 70 && nilai <= maxNilai)
            {
                Console.WriteLine("Kamu Berhasil");
                if (nilai == 100)
                {
                    Console.WriteLine("Kamu KEREN!");
                }
            }
            else if (0 >= nilai && nilai < 70) {
                Console.WriteLine("Kamu Gagal");
            }
            else {
                Console.WriteLine("Masukan Angka Yang Benar");
            }
        }

        static void elseStatement() {

            Console.Write("---Else Statement---");
            int a, b;
            Console.Write("Masukan Nilai a = ");
            a = int.Parse(Console.ReadLine());
            Console.Write("Masukan Nilai b = ");
            b = int.Parse(Console.ReadLine());

            if (a == b) {
                Console.WriteLine("Nilai a sama dengan b");
            }

            else if (a > b)
            {
                Console.WriteLine("Nilai a lebih besar dari b");
            }
            else{
                Console.WriteLine("Nilai a lebih kecil dari b");
            }
        }

        static void ifStatement() {

            Console.WriteLine("---If Statement---");
            int x, y;
            Console.Write("Masukan Nilai x = ");
            x = int.Parse(Console.ReadLine());
            Console.Write("Masukan Nilai y = ");
            y = int.Parse(Console.ReadLine());

            if (x > y ) {
                Console.WriteLine("x lebih besar dari nilai y");
            }
            if (y > x) {
                Console.Write("Nilai y lebih besar dari nilai x");
            }
            if (x == y) {
                Console.WriteLine("Nilai x sama dengan nilai y");
            }

        }

        static void impFashion() {
            int kodeBaju, harga = 0;
            char kodeUkuran;
            string merekBaju = "";

            Console.Write("Masukan Kode Baju (1, 2, 3) = ");
            kodeBaju = int.Parse(Console.ReadLine());

            Console.Write("Masukan Kode Ukuran (S, M, L, XL) = ");
            kodeUkuran = char.Parse(Console.ReadLine().ToUpper());

            if (kodeBaju == 1)
            {
                merekBaju = "IMP";
                if (kodeUkuran == 'S')
                {
                    harga = 200000;
                }
                else if (kodeUkuran == 'M')
                {
                    harga = 220000;
                }
                else
                {
                    harga = 250000;
                }
            }
            else if (kodeBaju == 2)
            {
                merekBaju = "Prada";
                if (kodeUkuran == 'S')
                {
                    harga = 150000;
                }
                else if (kodeUkuran == 'M')
                {
                    harga = 160000;
                }
                else
                {
                    harga = 170000;
                }
            }
            else if (kodeBaju == 3)
            {
                merekBaju = "Gucci";
                if (kodeUkuran == 'S' || kodeUkuran == 'M')
                {
                    harga = 200000;
                }
                else
                {
                    harga = 200000;
                }
            }
            else
            {
                Console.WriteLine("Masukan Input Dengan Benar");
            }

            Console.WriteLine($"Merek Baju = {merekBaju}");
            Console.WriteLine($"harga = {harga}");
        }
    }
}
