--SQL Day 02

--CAST
SELECT CAST(10 AS decimal(18,4))
SELECT CAST('10' AS int)
SELECT CAST(10.65 AS int)
SELECT CAST('2023-03-16' AS datetime)
SELECT GETDATE() AS HARI_INI, GETUTCDATE()
SELECT DAY(GETDATE()),MONTH(GETDATE()),YEAR(GETDATE())

--CONVERT
SELECT CONVERT(decimal(18,4), 10)
SELECT CONVERT(int, '10')
SELECT CONVERT(int, 10.65)
SELECT CONVERT(datetime, '2023-03-16')

--DATEADD
SELECT DATEADD(year, 2, GETDATE()), DATEADD(month, 3, GETDATE()), DATEADD(day, 5, GETDATE())

--DATEDIFF
SELECT DATEDIFF(DAY, '2023-03-16','2023-03-25'), DATEDIFF(MONTH, '2023-03-16', '2024-06-16')
SELECT DATEDIFF(YEAR, '2023-03-16', '2030-03-16')

--SUB QUERY
SELECT name,address,email,panjang FROM mahasiswa
WHERE panjang = (select MAX(panjang) FROM mahasiswa)

SELECT name, MAX(panjang) FROM mahasiswa
GROUP BY name

INSERT INTO [dbo].[mahasiswa]
SELECT name,address,email,panjang FROM mahasiswa

--CREATE VIEW
CREATE VIEW vwMahasiswa
AS
SELECT * FROM mahasiswa

--SELECT VIEW
SELECT * FROM vwMahasiswa

--DELETE VIEW
DROP VIEW vwMahasiswa

--CREATE INDEX
CREATE INDEX index_name
ON mahasiswa(name)

CREATE INDEX index_address_email
ON mahasiswa(address,email)

--CREATE UNIQUE INDEX
CREATE UNIQUE INDEX uniqueindex_panjang
ON mahasiswa(panjang)

--DROP INDEX
DROP INDEX index_address_email ON mahasiswa

--DROP UNIQUE INDEX
DROP INDEX uniqueindex_panjang ON mahasiswa

--ADD PRIMARY KEY
ALTER TABLE mahasiswa 
ADD CONSTRAINT pk_id_address PRIMARY KEY(id,address)

--DROP PRIMARY KEY
ALTER TABLE mahasiswa DROP CONSTRAINT pk_address

--ADD UNIQUE CONSTRAINT
ALTER TABLE mahasiswa 
ADD CONSTRAINT unique_address UNIQUE(address)

ALTER TABLE mahasiswa 
ADD CONSTRAINT unique_panjang UNIQUE(panjang)

UPDATE mahasiswa SET panjang = NULL WHERE id=5

--DROP UNIQUE CONSTRAINT
ALTER TABLE mahasiswa DROP CONSTRAINT unique_panjang