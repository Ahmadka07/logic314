﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logic07
{
    public class Mobil
    {
        public double kecepatan;
        public double bensin;
        public double posisi;
        public string nama;
        public string platno;

        public void utama()
        {
            Console.WriteLine("Start");
            Console.WriteLine("Nama = " + nama);
            Console.WriteLine("Plat Nomor = " + platno);
            Console.WriteLine("Bensin = " + bensin);
            Console.WriteLine("Kecepatan = " + kecepatan);
            Console.WriteLine("Posisi = " + posisi);
        }
    }
}

