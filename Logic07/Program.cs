﻿using System;
using System.Collections.Generic;

namespace Logic07
{
    class Mamalia
    {
        public virtual void pindah()
        {
            Console.WriteLine("Kucing Lari...");
        }
    }

    class Kucing : Mamalia
    {

    }

    class Paus : Mamalia
    {
        public override void pindah()
        {
            Console.WriteLine("Paus Berenang...");
        }
    }

    public class Program
    {
        static readonly List<Users> listUser = new List<Users>();
        static void Main(string[] args)
        {
            //bool ulangi = true;

            //while (ulangi)
            //{
            //    insertUser();
            //    Console.Write("\n");
            //}

            //listUser1();
            //dateTime();
            //stringDateTime();
            //timeSpan();
            //classInheritance();
            //overriding();

            Console.ReadKey();
        }

        static void listUser1()
        {
            Console.WriteLine("---List User---");
            List<Users> listUser = new List<Users>()
            {
                new Users(){Nama = "Firdha ", Umur = 24, Alamat = " Tangsel"},
                new Users(){Nama = "Isni ", Umur = 22, Alamat = " Cimahi"},
                new Users(){Nama = "Asti ", Umur = 23, Alamat = " Garut"},
                new Users(){Nama = "Muafa ", Umur = 22, Alamat = " Bogor"},
                new Users(){Nama = "Toni ", Umur = 24, Alamat = " Garut"}
            };

            for (int i = 0; i < listUser.Count; i++)
            {
                Console.WriteLine($"Nama = {listUser[i].Nama}\t Umur = {listUser[i].Umur} \t Alamat = {listUser[i].Alamat}");
            }

            //foreach (var item in listUser)
            //{
            //    Console.WriteLine(item.Nama + item.Umur + item.Alamat);
            //}
        }

        static void insertUser()
        {
            Console.WriteLine("---Insert User---");

            //List<Users> listUser = new List<Users>()
            //{
            //    new Users(){Nama = "Firdha ", Umur = 24, Alamat = " Tangsel"},
            //    new Users(){Nama = "Isni ", Umur = 22, Alamat = " Cimahi"},
            //    new Users(){Nama = "Asti ", Umur = 23, Alamat = " Garut"},
            //    new Users(){Nama = "Muafa ", Umur = 22, Alamat = " Bogor"},
            //    new Users(){Nama = "Toni ", Umur = 24, Alamat = " Garut"}
            //};

            Console.Write("Masukan Nama = ");
            string nama = Console.ReadLine();
            Console.Write("Masukan Umur = ");
            int umur = int.Parse(Console.ReadLine());
            Console.Write("Masukan Alamat = ");
            string alamat = Console.ReadLine();

            //listUser.Add(new Users() {Nama = nama, Umur = umur,  Alamat = alamat});

            Users user = new Users();
            user.Nama = nama;
            user.Umur = umur;
            user.Alamat = alamat;

            listUser.Add(user);

            for (int i = 0; i < listUser.Count; i++)
            {
                Console.WriteLine($"Nama = {listUser[i].Nama}\t Umur = {listUser[i].Umur} \t Alamat = {listUser[i].Alamat}");
            }

            //foreach (var item in listUser)
            //{
            //    Console.WriteLine(item.Nama + item.Umur + item.Alamat);
            //}
        }

        static void dateTime()
        {
            Console.WriteLine("---Date Time---");

            DateTime dt1 = new DateTime();
            Console.WriteLine(dt1);

            DateTime dt2 = DateTime.Now;
            Console.WriteLine(dt2);

            DateTime dt3 = DateTime.Now.Date;
            Console.WriteLine(dt3);

            DateTime dt4 = DateTime.UtcNow;
            Console.WriteLine(dt4);

            DateTime dt5 = new DateTime(2023, 3, 9);
            Console.WriteLine(dt5);

            DateTime dt6 = new DateTime(2023, 3, 9, 11, 45, 00);
            Console.WriteLine(dt6);

        }

        static void stringDateTime()
        {
            Console.WriteLine("---String DateTime--");

            Console.Write("Masukan tanggal(dd/mm/yyyy) = ");
            string strTanggal = Console.ReadLine();

            DateTime tanggal = DateTime.Parse(strTanggal);

            Console.WriteLine($"Hari = {tanggal.Day}");
            Console.WriteLine($"Bulan = {tanggal.Month}");
            Console.WriteLine($"Tahun = {tanggal.Year}");

            Console.WriteLine($"DayOfWeek ke = {(int)tanggal.DayOfWeek}");
            Console.WriteLine($"DayOfWeek ke = {tanggal.DayOfWeek}");
        }

        static void timeSpan()
        {
            Console.WriteLine("---Time Span---");

            DateTime date1 = new DateTime(2023, 3, 8, 0, 0, 0);
            DateTime date2 = DateTime.Now;

            //TimeSpan span = date1.Subtract(date2);
            TimeSpan span = date2 - date1;

            Console.WriteLine($"Total Hari = {span.Days}");
            Console.WriteLine($"Total Hari = {span.TotalDays}");
            Console.WriteLine($"Total Jam = {span.Hours}");
            Console.WriteLine($"Total Jam = {span.TotalHours}");
            Console.WriteLine($"Total Menit = {span.Minutes}");
            Console.WriteLine($"Total Menit = {span.TotalMinutes}");
            Console.WriteLine($"Total Detik = {span.Seconds}");
            Console.WriteLine($"Total Detik = {span.TotalSeconds}");
        }

        static void classInheritance()
        {
            TypeMobil typeMobil = new TypeMobil();
            typeMobil.kecepatan = 100;
            typeMobil.posisi = 50;
            typeMobil.Civic();

        }

        static void overriding()
        {
            Paus paus = new Paus();
            paus.pindah();

            Kucing kucing = new Kucing();
            kucing.pindah();
        }
    }
}
