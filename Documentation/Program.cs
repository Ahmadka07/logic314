﻿using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;

namespace Documentation
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.Title = "Documentation";

            //inputOutput();
            //convertTipeData();
            //operatorAritmatika();
            //operatorPenugasan();
            //operatorPerbandingan();
            //operatorLogika();
            //ifStatement();
            //ifElseStatement();
            //ifNested();
            //ternary();
            //Switch();
            //whileLoop();
            //doWhileLoop();
            //forLoopIncrement();
            //forLoopDecrement();
            //forLoopBreak();
            //forLoopContinue();
            //nestedFor();
            //arrayStatic();
            //arrayFor();
            //arrayForEach();
            //arrayDuaDimensi();
            //arrayDuaDimensiFor();
            //arrayTigaDimensiFor();
            //charArray();
            //convertArray();
            //arrayIndex();
            //splitJoin();
            //subString();
            //insertString();
            //replaceString();
            //removeString();
            //contain();
            //padLeftRight();

            Console.ReadKey();
        }

        static void inputOutput()
        {
            // Fungsi input dan output di C#
            Console.Write("Masukan Nama : ");
            string nama = Console.ReadLine();

            Console.WriteLine("Nama kamu adalah " + nama);
            Console.WriteLine("Nama anda adalah {0}", nama);
            Console.WriteLine($"Kamu bernama {nama}");

            /* Membuat variable kosong
            string alamat;
            int umur; 
            */

            // Membuat variable dengan inisialisasi
            string name = "Ahmad khoirul anwar";

            // Membuat variable yang belum diketahui tipe datanya
            var age = 24;

            // Membuat variable yang tidak dapat diubah nilainya
            const string address = "Ragunan";

            Console.Write("\n");

            Console.WriteLine("Nama"+"\t"+"= " + name);             // Nama     = Ahmad khoirul anwar
            Console.WriteLine("Umur{0}{1}{2}", "\t", "= ", age);    // Umur     = 24
            Console.WriteLine($"Alamat{"\t"}= {address}");          // Alamar   = Ragunan
        }

        static void convertTipeData()
        {
            Console.WriteLine("---Convert Tipe Data---");

            int umur = 24;
            string newUmur = umur.ToString();
            int newUmur2 = int.Parse(newUmur);
            Console.WriteLine(newUmur);

            int myInt = 10;
            double myDouble = 20.5;
            bool myBool = false;

            int myInt2 = Convert.ToInt32(myDouble);         // 20,5 = 20
            double myDouble2 = Convert.ToInt32(myInt);      // 10 = 10
            string myString = Convert.ToString(myInt);      // 10 = "10" cara 1
            string myString2 = myInt.ToString();            // 10 = "10" cara 2
            string myBool2 = Convert.ToString(myBool);      // false = "false"

            Console.WriteLine("Convert double to int = " +myInt2);
            Console.WriteLine("Convert int to double = {0}", myDouble2);
            Console.WriteLine($"Convert int to string = {myString}");
            Console.WriteLine($"Convert int to string = {myString2}");
            Console.WriteLine($"Convert bool to string = {myBool2}");
        }

        static void operatorAritmatika()
        {
            Console.WriteLine("---Operator Aritmatika---");
            
            int mangga, apel;

            Console.Write("Masukan jumlah mangga {0}= ", "\t\t");
            mangga = int.Parse(Console.ReadLine());                                 //10
            Console.Write("Masukan jumlah apel "+ "\t\t" +"= ");
            apel = int.Parse(Console.ReadLine());                                   //5

            int hasilTambah = mangga + apel;
            Console.WriteLine($"Total buah mangga + apel{"\t"}= {hasilTambah}");                    // 10 + 5 = 15
            int hasilKurang = mangga - apel;
            Console.WriteLine($"Total buah mangga - apel{"\t"}= {hasilKurang}");                    // 10 - 5 = 5
            int hasilKali = mangga * apel;
            Console.WriteLine($"Total buah mangga * apel{"\t"}= {hasilKali}");                      // 10 * 5 = 50
            int hasilBagi = mangga / apel;
            Console.WriteLine($"Total buah mangga / apel{"\t"}= {hasilBagi}");                      // 10 / 5 = 2

            Console.WriteLine($"Total buah mangga % apel{"\t"}= {hasilModulus(mangga, apel)}");     // 10 % 5 = 0
        }
        static int hasilModulus(int x, int y)
        {
            int hasil;
            hasil = x % y;
            return hasil;
        }

        static void operatorPenugasan()
        {
            Console.WriteLine("---Operator Penugasan---");

            int mangga = 5;
            int apel = 5;
            Console.WriteLine($"Mangga {"\t\t\t\t\t"}= {mangga}");

            Console.Write("\n");

            mangga = 10; // Di inisialisasi ulang
            Console.WriteLine($"Mangga setelah di inisialisasi ulang{"\t"}= {mangga}");
            Console.WriteLine($"Apel{"\t\t\t\t\t"}= {apel}");

            apel += mangga;                                             // 5 + 10 = 15                                       
            Console.WriteLine($"Apel += mangga{"\t\t\t\t"}: {apel}");
            apel -= mangga;                                             // 15 - 10 = 5
            Console.WriteLine($"Apel -= mangga{"\t\t\t\t"}: {apel}");
            apel *= mangga;                                             // 5 * 10 = 50
            Console.WriteLine($"Apel *= mangga{"\t\t\t\t"}: {apel}");
            apel /= mangga;                                             // 50 / 10 = 5
            Console.WriteLine($"Apel /= mangga{"\t\t\t\t"}: {apel}");
            apel %= mangga;                                             // 5 % 10 = 5
            Console.WriteLine($"Apel %= mangga{"\t\t\t\t"}: {apel}");
        }

        static void operatorPerbandingan()
        {
            Console.WriteLine("---Operator Perbandingan---");

            int mangga, apel;
            Console.Write($"Masukan jumlah mangga{"\t"}= ");
            mangga = int.Parse(Console.ReadLine());                         // 10
            Console.Write($"Masukan jumlah apel{"\t"}= ");
            apel = int.Parse(Console.ReadLine());                           // 5

            Console.WriteLine($"Mangga > Apel{"\t\t"}= {mangga > apel}");     // True
            Console.WriteLine($"Mangga < Apel{"\t\t"}= {mangga < apel}");     // False
            Console.WriteLine($"Mangga >= Apel{"\t\t"}= {mangga >= apel}");   // True
            Console.WriteLine($"Mangga <= Apel{"\t\t"}= {mangga <= apel}");   // False
            Console.WriteLine($"Mangga == Apel{"\t\t"}= {mangga == apel}");   // False
            Console.WriteLine($"Mangga != Apel{"\t\t"}= {mangga != apel}");    // True
        }

        static void operatorLogika()
        {
            Console.WriteLine("---Operator Logika---");

            Console.Write($"Masukan Umur{"\t\t"}= ");
            int umur = int.Parse(Console.ReadLine());
            Console.Write($"Masukan Password{"\t"}= ");
            string password = Console.ReadLine();

            bool isAdult = umur > 18;
            bool isPassword = password == "admin";

            if (isAdult && isPassword)
            {
                Console.WriteLine("Welcome to the club!");
            }
            else if (isAdult && !isPassword)
            {
                Console.WriteLine("Anda sudah dewasa dan password salah");
            }
            else if (!isAdult && isPassword)
            {
                Console.WriteLine("Anda belum dewasa dan password benar");
            }
            else
            {
                Console.WriteLine("Anda belum dewasa dan password salah");
            }
        }

        static void ifStatement()
        {
            Console.WriteLine("---If Statement---");

            int x, y;

            Console.Write($"Masukan nilai X{"\t"}= ");
            x = int.Parse(Console.ReadLine());
            Console.Write($"Masukan nilai Y{"\t"}= ");
            y = int.Parse(Console.ReadLine());

            if (x > y)
            {
                Console.WriteLine("Nilai X lebih besar dari Y");
            }
            if (x < y)
            {
                Console.WriteLine("Nilai Y lebih besar dari X");
            }
            if (x == y)
            {
                Console.WriteLine("Nilai X dan Y sama besar");
            }
        }

        static void ifElseStatement()
        {
            Console.WriteLine("---If Else Statement---");

            Console.Write($"Masukan Nilai X{"\t"}= ");
            int x = int.Parse(Console.ReadLine());
            Console.Write($"Masukan Nilai Y{"\t"}= ");
            int y = int.Parse(Console.ReadLine());

            if (x > y)
            {
                Console.WriteLine("Nilai X lebih besar dari Nilai Y");
            }
            else if (x < y)
            {
                Console.WriteLine("Nilai X lebih kecil dari Nilai Y");
            }
            else
            {
                Console.WriteLine("Nilai X dan Nilai Y sama besar");
            }
        }

        static void ifNested()
        {
            Console.WriteLine("---If Nested---");

            Console.Write($"Masukan nilai anda{"\t"}= ");
            int nilai = int.Parse(Console.ReadLine());

            if (nilai >= 0 && nilai <= 100)
            {
                if (nilai >= 90 && nilai < 100)
                {
                    Console.WriteLine("Kamu luar biasa");
                }
                else if (nilai == 100)
                {
                    Console.WriteLine("Kamu sempurna");
                }
                if (nilai >= 78 && nilai <= 89)
                {
                    Console.WriteLine("Kamu berhasil");
                }
            }
            else if (nilai >= 0 && nilai <= 77)
            {
                Console.WriteLine("Kamu gagal");
            }
            else
            {
                Console.WriteLine("Masukan nilai yang benar!");
            }
        }

        static void ternary()
        {
            Console.WriteLine("---Ternary---");

            Console.Write($"Masukan nilai X{"\t"}= ");
            int x = int.Parse(Console.ReadLine());
            Console.Write($"Masukan nilai Y{"\t"}= ");
            int y = int.Parse(Console.ReadLine());

            string hasil = (x > y) ? "Nilai X lebih besar dari nilai Y" : (x < y) ?"Nilai X lebih kecil dari nilai Y" : "X dan Y bernilai sama";
            Console.WriteLine(hasil);
        }

        static void Switch()
        {
            Console.WriteLine("---Switch---");

            Console.Write($"Masukan buah kesukaan kamu{"\t"}= ");
            string buah = Console.ReadLine().ToLower();

            switch(buah)
            {
                case "pisang":
                    Console.WriteLine($"Buah kesukaan kamu {buah}");
                    break;
                case "apel":
                    Console.WriteLine($"Buah kesukaan kamu {buah}");
                    break;
                case "semangka":
                    Console.WriteLine($"Buah kesukaan kamu {buah}");
                    break;
                default:
                    Console.WriteLine("Buah kesukaan kamu belum tersedia");
                    break;
            }
        }

        static void whileLoop()
        {
            Console.WriteLine("---While Loop---");

            bool loop = true;
            int nilai = 1;

            while (loop)
            {
                Console.WriteLine($"Proses ke = {nilai}");
                Console.Write($"Ulangi proses kembali(y / n){"\t"}? ");
                string input = Console.ReadLine().ToLower();

                if (input == "y")
                {
                    nilai++;
                }
                else if (input == "n")
                {
                    loop = false;
                }
                else
                {
                    Console.WriteLine("Pilih y/n !");
                }
            }
        }

        static void doWhileLoop()
        {
            Console.WriteLine("---Do While Loop---");

            int a = 0;

            do
            {
                Console.WriteLine($"{a}");
                a++;
            }
            while (a < 5);
        }

        static void forLoopIncrement()
        {
            Console.WriteLine("---For Loop Increment---");

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }
        }

        static void forLoopDecrement()
        {
            Console.WriteLine("---For Loop Decrement---");

            for (int i = 10; i > 0; i--)
            {
                Console.WriteLine(i);
            }
        }

        static void forLoopBreak()
        {
            Console.WriteLine("---For Loop Break---");

            for (int i = 0; i < 10; i++)
            {
                if (i == 5)
                {
                    break;
                }
                Console.WriteLine(i);
            }
        }

        static void forLoopContinue()
        {
            Console.WriteLine("---For Loop Continue---");

            for (int i = 0; i < 10; i ++)
            {
                if (i == 5)
                {
                    continue;
                }
                Console.WriteLine(i);
            }
        }

        static void nestedFor()
        {
            Console.WriteLine("---Nested For---");

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.Write($"[{i},{j}]");
                }
                Console.Write("\n");
            }
        }

        static void arrayStatic()
        {
            Console.WriteLine("---Array Static---");

            int[] staticArray = new int[3];

            // Mengisi Array

            staticArray[0] = 1;
            staticArray[1] = 2;
            staticArray[2] = 3;

            // Cetak Array

            Console.WriteLine(staticArray[0]);
            Console.WriteLine(staticArray[1]);
            Console.WriteLine(staticArray[2]);
        }

        static void arrayFor()
        {
            Console.WriteLine("---Array For---");

            int[] angkaArray = new int[]
            {
                1, 2, 3, 4
            };

            for (int i = 0; i < angkaArray.Length; i++)
            {
                Console.WriteLine(angkaArray[i]);   
            }

            Console.Write("\n");

            string[] namaArray = new string[]
            {
                "Abdullah Muafa",
                "Isni Dwitiniarti",
                "Ilham Rizki",
                "Marcellino",
                "Alwi Siregar"
            };

            for (int j = 0; j < namaArray.Length; j++)
            {
                Console.WriteLine(namaArray[j]);
            }
        }

        static void arrayForEach()
        {
            Console.WriteLine("---Array Foreach---");

            string[] namaArray = new string[]
            {
                "Abdullah Muafa",
                "Isni Dwitiniarti",
                "Ilham Rizki",
                "Marcellino",
                "Alwi Siregar"
            };

            foreach (string nama in namaArray)
            {
                Console.WriteLine(nama);
            }

        }

        static void arrayDuaDimensi()
        {
            Console.WriteLine("---Array Dua Dimensi---");

            int[,] array = new int[,]
            {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
            };

            Console.WriteLine(array[0, 1]); // 2
            Console.WriteLine(array[1, 2]); // 6
        }

        static void arrayDuaDimensiFor()
        {
            Console.WriteLine("---Array Dua Dimensi For---");

            int[,] array = new int[,]
            {
                {1, 2},
                {3, 4},
                {5, 6,}
            };

            Console.WriteLine(array.Length); // 6
            Console.WriteLine(array.GetLength(0)); // 3 (Rows)
            Console.WriteLine(array.GetLength(1)); // 2 (Columns)
            Console.WriteLine(array.Rank); // 2

            Console.Write("\n");

            for (int i = 0; i <= array.Rank; i++)
            {
                for (int j = 0; j < array.Rank; j++)
                {
                    Console.Write(array[i,j] + " ");
                }
                Console.Write("\n");
            }
        }

        static void arrayTigaDimensiFor()
        {
            Console.WriteLine("---Array Tiga Dimensi For---");

            int[,,] array3Da = new int[2, 2, 3] {
                                                    {
                                                        { 1, 2, 3 },
                                                        { 4, 5, 6 }
                                                    },
                                                    {
                                                        { 7, 8, 9 },
                                                        { 10, 11, 12 }
                                                    }
            };

            Console.WriteLine(array3Da.Length); // 12
            Console.WriteLine(array3Da.Rank); // 3
            Console.WriteLine(array3Da.GetLength(0)); // 2 (Dimesions)
            Console.WriteLine(array3Da.GetLength(1)); // 2 (Rows)
            Console.WriteLine(array3Da.GetLength(2)); // 3 (Columns)

            Console.Write("\n");

            for (int i = 0; i < array3Da.GetLength(0); i++)
            {
                for (int j = 0; j < array3Da.GetLength(1); j++)
                {
                    for (int k = 0; k < array3Da.GetLength(2); k++)
                    {
                        Console.Write(array3Da[i, j, k] + " ");
                    }
                    Console.Write("\n");
                }
                Console.Write("\n");
            }
        }

        static void charArray()
        {
            Console.WriteLine("---Char Array---");

            Console.Write($"Masukan Kalimat{"\t"}= ");
            
            //string kalimat = Console.ReadLine();
            //char[] huruf = kalimat.ToCharArray();

            char[] huruf = Console.ReadLine().ToCharArray();

            foreach (char i in huruf)
            {
                Console.WriteLine(i);
            }
        }

        static void convertArray()
        {
            Console.WriteLine("---Convert Array---");

            Console.Write($"Masukan angka(pakai koma){"\t\t"}= ");
            int[] array = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            Console.WriteLine(string.Join(" ", array));
        }

        static void arrayIndex()
        {
            List<int> list = new List<int>() { 3, 5, 2, 7, 6 };
            int[] arr = { 3, 5, 2, 7, 6 };
            int item = 2;

            int index = list.IndexOf(item);
            int idx = Array.IndexOf(arr, item);

            if (index != -1)
            {
                Console.WriteLine(String.Format("Element {0} is found at index {1}",item, index ));
                Console.WriteLine(idx);
            }
            else
            {
                Console.WriteLine("Element not found in the given list.");
            }
        }

        static void subString()
        {
            Console.WriteLine("---Sub String---");

            Console.Write($"Masukan kalimat{"\t"}= ");
            string kalimat = Console.ReadLine();

            Console.WriteLine($"Sub String (1, 4){"\t"}= {kalimat.Substring(1, 4)}");
            Console.WriteLine($"Sub String (5, 2){"\t"}= {kalimat.Substring(5, 2)}");
            Console.WriteLine($"Sub String (7, 9){"\t"}= {kalimat.Substring(7, 9)}");
            Console.WriteLine($"Sub String (9){"\t\t"}= {kalimat.Substring(9)}");
        }

        static void splitJoin()
        {
            Console.WriteLine("---Split dan Join---");

            Console.Write($"Masukan kalimat{"\t"}= ");
            string[] perKata = Console.ReadLine().Split(" ");

            foreach (string kata in perKata)
            {
                Console.WriteLine(kata);
            }
            Console.WriteLine(string.Join(",", perKata));
        }

        static void insertString()
        {
            Console.WriteLine("---Insert String---");

            Console.Write($"Masukan Kalimat{"\t\t"}= ");
            string kalimat = Console.ReadLine();
            Console.Write($"Isi parameter insert{"\t"}= ");
            int parameter = int.Parse(Console.ReadLine());
            Console.Write($"Input string insert{"\t"}= ");
            string input = " " + Console.ReadLine();

            Console.WriteLine($"Hasil insert string{"\t"}= {kalimat.Insert(parameter, input)}");
        }

        static void replaceString()
        {
            Console.WriteLine("---Replace String---");

            Console.Write($"Masukan Kalimat{"\t\t\t"}= ");
            string kalimat = Console.ReadLine();
            Console.Write($"Masukan kata yang akan diganti{"\t"}= ");
            string lama = Console.ReadLine();
            Console.Write($"Masukan kata pengganti{"\t\t"}= ");
            string baru = Console.ReadLine();

            Console.WriteLine($"Hasil replace string{"\t\t"}= {kalimat.Replace(lama, baru)}");
        }

        static void removeString()
        {
            Console.WriteLine("---Remove String---");

            Console.Write($"Masukan kalimat{"\t\t"}= ");
            string kalimat = Console.ReadLine();
            Console.Write($"Masukan parameter{"\t"}= ");
            int parameter = int.Parse(Console.ReadLine());

            Console.WriteLine($"Hasil remove string{"\t"}= {kalimat.Remove(parameter)}");
        }

        static void contain()
        {
            Console.WriteLine("---Contain---");

            Console.Write($"Masukan kalimat{"\t\t\t\t"}= ");
            string kalimat = Console.ReadLine();
            Console.Write($"Masukan contain(kata yang dicari){"\t"}= ");
            string contain = Console.ReadLine();

            Console.Write("\n");

            if (kalimat.Contains(contain))
            {
                Console.WriteLine($"Kalimat ({kalimat}) ini mengandung kata {contain}");
            }
            else
            {
                Console.WriteLine($"Kalimat ({kalimat}) ini tidak mengandung {contain}");
            }
        }

        static void padLeftRight()
        {
            Console.WriteLine("---Pad Left---");

            Console.Write($"Masukan input{"\t\t\t"}= ");
            int input = int.Parse(Console.ReadLine());
            Console.Write($"Masukan panjang karakter{"\t"}= ");
            int panjang = int.Parse(Console.ReadLine());
            Console.Write($"Masukan karakter yang akan dimasukan{"\t"}= ");
            char karakter = char.Parse(Console.ReadLine());

            Console.WriteLine($"Hasil Padleft{"\t\t"}= {input.ToString().PadLeft(panjang, karakter)}");
            Console.WriteLine($"Hasil Padleft{"\t\t"}= {input.ToString().PadRight(panjang, karakter)}");
        }

    }
}
