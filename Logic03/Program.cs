﻿using System;
using System.ComponentModel.Design;

namespace Logic03
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Console.Title = "Day 3";
            //soal1();
            //soal2();
            //soal3();
            //soal4();
            //perulanganWhile();
            //perulanganDoWhile();
            //perulanganForIncreement();
            //perulanganForDecreement();
            //Break();
            //Continue();
            //forBersarang();
            //arrayStatic();
            //arrayForEach();
            //arrayFor();
            //array2Dimensi();
            //array2DimensiFor();
            //splitJoin();
            //subString();
            stringToCharArray();

            Console.ReadKey();
        }

        static void soal1()
        {
            Console.WriteLine("---Program Untuk Mencari Angka Modulus");

            int angka, pembagi;

            Console.Write("Masukan Angka = ");
            angka = int.Parse(Console.ReadLine());

            Console.Write("Masukan Pembagi = ");
            pembagi = int.Parse(Console.ReadLine());
            
            
            if (angka % pembagi == 0)
            {
                Console.Write(angka);
                Console.Write(" % ");
                Console.Write(pembagi);
                Console.WriteLine(" Adalah 0");
            }
            else
            {
                Console.Write(angka);
                Console.Write(" % ");
                Console.Write(pembagi);
                Console.WriteLine(" bukan 0 melainkan hasil mod");
            }
        }

        static void soal2()
        {
            Console.WriteLine();
            Console.WriteLine("---Studi Kasus Pemulung Puntung Rokok---");

            int puntung, sisaMod, pembagi, penjualanBatangRokok;
            int batang = 8;
            int hargaRokok = 500;

            Console.Write("Jumlah Puntung Rokok yang di kumpulkan = ");
            puntung = int.Parse(Console.ReadLine());

            sisaMod = puntung % batang;
            pembagi = puntung / batang;
            penjualanBatangRokok = pembagi * hargaRokok;

            if (puntung <= 0)
            {
                Console.WriteLine("Silahkan Cari Puntung Rokok");
            }
            else
            {
                Console.WriteLine($"Jumlah batang rokok yang di dapat = {pembagi}");
                Console.WriteLine($"Sisa puntung rokok = {sisaMod}");
                Console.WriteLine($"Harga 1 batang rokok = {hargaRokok}");
                Console.WriteLine($"Penghasilan menjual batang rokok = {penjualanBatangRokok}");
            }
        }

        static void soal3()
        {
            Console.WriteLine();
            Console.WriteLine("---Studi Kasus Grade Nilai---");
            int nilai;
            int maxNilai = 100;

            Console.Write("Masukan Nilai = ");
            nilai = int.Parse(Console.ReadLine());

            if (nilai >= 80 && nilai < maxNilai)
            {
                Console.WriteLine("Grade A");
            }
            else if (nilai >= 60 && nilai < 80)
            {
                Console.WriteLine("Grade B");
            }
            else if (nilai >= 0 && nilai < 60)
            {
                Console.WriteLine("Grade C");
            }
            else
            {
                Console.WriteLine("Masukan Nilai yang Benar");
            }
        }

        static void soal4()
        {
            Console.WriteLine();
            Console.WriteLine("---Studi Kasus Ganjil Genap---");

            int angka;

            Console.Write("Masukan Angka = ");
            angka = int.Parse(Console.ReadLine());

            if (angka % 2 == 0 )
            {
                Console.WriteLine($"Angka {angka} adalah Genap");
            }
            else
            {
                Console.WriteLine($"Angka {angka} adalah Ganjil");
            }
        }

        static void perulanganWhile()
        {
            Console.WriteLine("---Perulangan While---");
            bool ulangi = true;
            int nilai = 1;

            while (ulangi)
            {
                Console.WriteLine($"proses ke : {nilai}");
                //nilai++ ;

                Console.Write("Apakah anda akan mengulangi proses? (y/n)");
                string input = Console.ReadLine().ToLower();

                if (input == "y")
                {
                    ulangi = true;
                    Console.WriteLine();
                }
                else if (input == "n")
                {
                    ulangi = false;
                }
                else
                {
                    Console.WriteLine("Input yang anda masukan salah");
                    Console.WriteLine();
                    nilai = 1;
                } 
            }
        }

        static void perulanganDoWhile()
        {
            int a = 0;
            do
            {
                Console.WriteLine(a);
                a++;
            }
            while (a < 5);
        }

        static void perulanganForIncreement()
        {
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(i);
            }
        }

        static void perulanganForDecreement()
        {
            for (int i = 10; i > 0; i--)
            {
                Console.WriteLine(i);
            }
        }

        static void Break()
        {
            for (int i = 0; i < 10; i++)
            {
                if (i == 5)
                {
                    break;
                }
                Console.WriteLine(i);
            }
        }
        
        static void Continue()
        {
            for(int i = 0; i < 10; i++)
            {
                if (i == 5)
                {
                    continue;
                }
                Console.WriteLine(i);
            }
        }

        static void forBersarang()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.Write($"[{i},{j}]");
                }
                Console.Write("\n");
            }
        }

        static void arrayStatic()
        {
            Console.WriteLine("---Array---");
            int[] staticIntArray = new int[3];
            //Mengisi array
            staticIntArray[0] = 1;
            staticIntArray[1] = 2;
            staticIntArray[2] = 3;

            //Cetak array
            Console.WriteLine(staticIntArray[0]);
            Console.WriteLine(staticIntArray[1]);
            Console.WriteLine(staticIntArray[2]);
        }

        static void arrayForEach()
        {
            string[] array = new string[]
            {
                "Abdullah Muafa",
                "Isni Dwitiniardi",
                "Ilham Rizki",
                "Marchelino",
                "Alwi fadli siregar"
            };
            
            //Baca array menggunakan foreach

            foreach (string item in array)
            {
                Console.WriteLine(item);
            }

        }

        static void arrayFor()
        {
            string[] array = new string[]
            {
                "Abdullah Muafa",
                "Isni Dwitiniardi",
                "Ilham Rizki",
                "Marchelino",
                "Alwi fadli siregar"
            };

            //Baca array menggunakan for

            for (int i = 0; i < array.Length; i++)
            {
                Console.WriteLine(array[i]);
            }

            for (int i = 1; i <= array.Length - 1; i++)
            {
                Console.WriteLine(array[i]);
            }

        }

        static void array2Dimensi()
        {
            int[,] array = new int[,]
            {
                { 1, 2, 3 },
                { 4, 5, 6 },
                { 7, 8, 9 }
            };

            Console.WriteLine(array[0, 1]); //2
            Console.WriteLine(array[1, 2]); //6
            Console.WriteLine(array[2, 1]); //8
            Console.WriteLine(array[2, 2]); //9
        }

        static void array2DimensiFor()
        {
            int[,] array = new int[,]
            {
                { 1, 2, 3 },
                { 4, 5, 6 },
                { 7, 8, 9 }
            };

            //Cetak menggunakan for
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.Write(array[i, j] + " ");
                }
                Console.Write("\n");
            }
        }

        static void splitJoin()
        {
            Console.WriteLine("---Split dan Join---");
            Console.Write("Masukan Kalimat = ");
            string kalimat = Console.ReadLine();
            string[] kataArray = kalimat.Split(" ");
            
            foreach (string kata in kataArray)
            {
                Console.WriteLine(kata);
            }

            Console.WriteLine(string.Join(",", kataArray));

        }

        static void subString()
        {
            Console.WriteLine("---Sub String---");
            Console.Write("Masukan Kalimat : ");
            string kalimat = Console.ReadLine();

            Console.WriteLine("Substring (1, 4) :" + kalimat.Substring(1, 4));
            Console.WriteLine("Substring (5, 2) :" + kalimat.Substring(5, 2));
            Console.WriteLine("Substring (7, 9) :" + kalimat.Substring(7, 9));
            Console.WriteLine("Substring (9) :" + kalimat.Substring(9));
        }

        static void stringToCharArray()
        {
            Console.WriteLine("---String ToCharArray");
            Console.Write("Masukan Kalimat = ");
            string kalimat = Console.ReadLine();

            char[] array = kalimat.ToCharArray();

            foreach (char ch in array)
            {
                Console.WriteLine(ch);
            }
        }
    }
}
