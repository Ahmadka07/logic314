﻿using System;

namespace Logic01
{
    public class Program
    {
        static void Main(string[] args)
        {
            //konversi();
            //operatorAritmatika();
            //modulus();
            //operatorPenugasan();
            //operatorPerbandingan();
            //operatorLogika();
            //testIfElse();
            //teknikalTestNamaHari();
            test();

            Console.ReadKey();
        }

        static void operatorLogika() {
            Console.Write("Masukan umur anda = ");
            int umur = int.Parse(Console.ReadLine());

            Console.Write("Masukan password anda = ");
            string password = Console.ReadLine();

            //Pernyataan 1
            bool isAdult = umur > 18;
            //Pernyataan 2
            bool isPasswordValid = password == "admin";

            if (isAdult && isPasswordValid) {
                Console.WriteLine("Anda sudah dewasa dan password valid");
            }
            else if (isAdult && !isPasswordValid) {
                Console.WriteLine("Anda sudah dewasa dan password invalid");
            }
            else if (!isAdult && isPasswordValid) {
                Console.WriteLine("Anda belum dewasa dan password valid");
            }
            else
            {
                Console.WriteLine("Anda belum dewasa dan password tidak valid");
            }
        }

        static void operatorPerbandingan()
        {
            int mangga, apel = 0;

            Console.WriteLine("---Operator Perbandingan---");

            Console.Write("Jumlah Mangga = ");
            mangga = int.Parse(Console.ReadLine());

            Console.Write("Jumlah Apel = ");
            apel = int.Parse((Console.ReadLine()));

            Console.WriteLine("--Hasil Perbandingan---");

            Console.WriteLine($"Mangga > Apel : {mangga > apel}");
            Console.WriteLine($"Mangga >= Apel : {mangga >= apel}");
            Console.WriteLine($"Mangga < Apel : {mangga < apel}");
            Console.WriteLine($"Mangga <= Apel : {mangga <= apel}");
            Console.WriteLine($"Mangga != Apel : {mangga != apel}");
            Console.WriteLine($"Mangga == Apel : {mangga == apel}");

        }

        static void operatorPenugasan()
        {
            int mangga = 10;
            int apel = 8;

            Console.WriteLine($"Mangga awal = {mangga}");
            Console.WriteLine($"Apel awal = {apel}");

            mangga = 15; //Mengisi ulang nilai variable mangga

            Console.WriteLine($"Mangga setelah di ubah nilai awalnya menjadi = {mangga}");

            apel += mangga;
            Console.WriteLine($"Penambahan dan penugasan(apel += mangga) = {apel}");

            mangga = 15;
            apel = 8;
            apel -= mangga;
            Console.WriteLine($"Pengurangan dan penugasan(apel -= mangga) = {apel}");

            mangga = 15;
            apel = 8;
            apel *= mangga;
            Console.WriteLine($"Perkalian dan penugasan (apel *= mangga) = {apel}");

            mangga = 15;
            apel = 8;
            apel /= mangga;
            Console.WriteLine($"Pembagian dan penugasan (apel /= mangga) = {apel}");

            mangga = 15;
            apel = 8;
            Console.WriteLine($"Modulus dan penugasan (apel %= mangga) = {apel}");
        }

        static void modulus()
        {
            int mangga, apel, hasil = 0;

            Console.Write("Jumlah mangga = ");
            mangga = Convert.ToInt32(Console.ReadLine());

            Console.Write("Jumlah aple = ");
            apel = int.Parse(Console.ReadLine());

            hasil = mangga % apel;
            Console.WriteLine($"Hasil penjumlahan mangga % apel = {hasil}");

        }

        static void operatorAritmatika()
        {

            int mangga, apel;

            Console.WriteLine("---Method ReturnType---");
            Console.Write("Jumlah mangga = ");
            mangga = Convert.ToInt32(Console.ReadLine());

            Console.Write("Jumlah aple = ");
            apel = int.Parse(Console.ReadLine());

            int hasil = hitungJumlah(mangga, apel);
            
            Console.WriteLine($"Hasil penjumlahan mangga + apel = {hasil}");

        }

        static int hitungJumlah(int mangga, int apel) {
            int hasil = 0;
            hasil = mangga + apel;
            return hasil;
        }

        static void konversi()
        {

            int umur = 24;
            string strUmur = umur.ToString();
            int umur2 = int.Parse(strUmur);


            int myInt = 10;
            double myDouble = 5.25;
            bool myBool = false;

            string myString = Convert.ToString(myInt);
            double myConvDouble = Convert.ToDouble(myInt);
            int myConvInt = Convert.ToInt32(myDouble);
            string myConvString = Convert.ToString(myBool);

            Console.WriteLine("Convert int to string : " + myString);
            Console.WriteLine("Convert int to double : " + myConvDouble);
            Console.WriteLine("Convert double to int : " + myConvInt);
            Console.WriteLine("Convert bool to string : " + myConvString);

            Console.WriteLine(strUmur);
            Console.WriteLine(umur2);

        }

        static void testIfElse()
        {

            int mtk, fisika, kimia;

            Console.Write("Masukan Nilai MTK = ");
            mtk = int.Parse(Console.ReadLine());
            Console.Write("Masukan Nilai Fisika = ");
            fisika = int.Parse(Console.ReadLine());
            Console.Write("Masukan Nilai Kimia = ");
            kimia = int.Parse(Console.ReadLine());

            int total = rataRata(mtk, fisika, kimia);
            Console.WriteLine($"Nilai Rata-Rata = {total}");

            if (total > 75)
            {
                Console.WriteLine("Selamat");
                Console.WriteLine("Kamu berhasil");
                Console.WriteLine("Kamu Hebat");
            }
            else
            {
                Console.WriteLine("Maaf");
                Console.WriteLine("Kamu gagal");
            }
        }
        
        static int rataRata(int mtk, int fisika, int kimia)
        {
            int total = 0;
            total = (mtk + fisika + kimia) /3;
            return total;
        }

        static void teknikalTestNamaHari ()
        {
            string[] hari = new string[]
            {
                "Minggu",
                "Senin",
                "Selasa",
                "Rabu",
                "Kamis",
                "Jumat",
                "Sabtu"
            };

            Console.Write("Input angka untuk mengetahui hari = ");
            int input = int.Parse(Console.ReadLine());
            input %= 7;

            Console.WriteLine(hari[input]);

        }

        static void test ()
        {
            Console.Write("Masukan Kalimat = ");
            string text = Console.ReadLine();

            char indexArray = text[0];
            Console.WriteLine(indexArray);
            Console.WriteLine(text[0]);


        }
    }
}
