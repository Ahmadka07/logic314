﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Globalization;
using System.Linq;

namespace Simulasi_Logic
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soal01();
            //soal02();
            //soal03();
            //soal04();
            //soal05();
            //soal06();
            //soal06Ver2();
            //soal07();
            //soal08();
            //soal09();
            //soal10();
            //soalKisi01();
            //soalKisi02();
            //soalRekursif(); // Fungsi yang manggil dirinya sendiri
            //soalRekursifAsc();
            Console.ReadKey();
        }

        static void soal01()
        {
            Console.WriteLine("---Soal Nomor 1---");
            Console.WriteLine("Contoh Output : AkuSayangKamuTapiKamu || 5");

            string input = "AkuSayangKamuTapiKamu";
            char[] array = input.ToCharArray();
            int hitung = 0;

            for (int i = 0; i < array.Length; i++)
            {
                if (Char.IsUpper(array[i]))
                {
                    hitung++;
                }
            }
            Console.WriteLine(hitung);
        }

        static void soal02()
        {
            Console.WriteLine("---Soal Nomor 2--");
            Console.WriteLine("Contoh Output : Start = 10, End = 15 || XA-07082022-00010 s/d XA-07082022-00015");
            Console.Write("\n");

            Console.Write("Masukan Start = ");
            int start = int.Parse(Console.ReadLine());
            Console.Write("Masukan End = ");
            int end = int.Parse(Console.ReadLine());

            string inisial = "XA";
            string hari = "07";
            string bulan = "08";
            string tahun = "2022";
            Console.Write("\n");

            for (int i = start; i <= end; i++)
            {
                Console.WriteLine($"{inisial}-{hari}{bulan}{tahun}-{i.ToString().PadLeft(5, '0')}");
            }
        }

        static void soal03()
        {
            Console.WriteLine("---Soal Nomor 3---");
            Console.WriteLine("Terdapat 3 buah keranjang buah di dapur.");
            Console.WriteLine("Salah satu Keranjang kosong dan sisanya berisi masing-masing n&m buah apabila salah satu keranjang tersebut dibawa ke pasar.");
            Console.WriteLine("Maka berapakah jumlah buah yanga ada di dapur sekarang?");
            Console.WriteLine("Contoh Output : keranjang 1 = 0, keranjang 2 = 2, keranjang 3 = 10, keranjang 1 dibawa ke pasar");
            Console.WriteLine("Sisa buah = 13");
            Console.Write("\n");

            Console.Write("Masukan jumlah bauh di keranjang 1 = ");
            int keranjang1 = int.Parse(Console.ReadLine());
            Console.Write("Masukan jumlah bauh di keranjang 2 = ");
            int keranjang2 = int.Parse(Console.ReadLine());
            Console.Write("Masukan jumlah bauh di keranjang 3 = ");
            int keranjang3 = int.Parse(Console.ReadLine());
            Console.Write("Pilih keranjang yang akan dibawa ke pasar = ");
            int pilih = int.Parse(Console.ReadLine());
            Console.Write("\n");

            if (pilih == 1)
            {
                Console.WriteLine($"sisa buah = {keranjang2 + keranjang3}");
            }
            else if (pilih == 2)
            {
                Console.WriteLine($"sisa buah = {keranjang1 + keranjang3}");
            }
            else if (pilih == 3)
            {
                Console.WriteLine($"sisa buah = {keranjang1 + keranjang2}");
            }
            else
            {
                Console.WriteLine("pilihan tidak tersedia");
            }
        }

        static void soal04()
        {
            Console.WriteLine("---Soal Nomor 4---");
            Console.WriteLine("di sebuah musibah banjir disuatu tempat.");
            Console.WriteLine("Didalam tempat pengungsian terdapat beberapa orang yg butuh bantuan pakaian maka dari itu.");
            Console.WriteLine("Setiap laki laki dewasa mendapatkan 1 Baju, setiap wanita dewasa mendapatkan 2 baju.");
            Console.WriteLine("setiap anak anak mendapatkan 3 baju. Dan setiap bayi mendapatkan 5 baju.");
            Console.WriteLine("Berapa total baju yang diberikan?");
            Console.WriteLine("Jika total baju ganjil dan lebih dari 10 maka setiap wanita dewasa mendapatkan masing-masing 1 baju lagi");
            Console.Write("\n");

            Console.WriteLine("Laki dewasa = 5, wanita dewasa = 3, anak2 = 3, Bayi = 1 || 30 Baju");

            Console.Write("Laki dewasa = ");
            int lakiDewasa = int.Parse(Console.ReadLine()); lakiDewasa *= 1;
            Console.Write("Wanita dewasa = ");
            int wanitaDewasa = int.Parse(Console.ReadLine()); wanitaDewasa *= 2;
            Console.Write("Anak2 = ");
            int anak2 = int.Parse(Console.ReadLine()); anak2 *= 3;
            Console.Write("Bayi = ");
            int bayi = int.Parse(Console.ReadLine()); bayi *= 5;

            int total = lakiDewasa + wanitaDewasa + anak2 + bayi;

            if (total % 2 != 0)
            {
                total += (wanitaDewasa / 2);
            }
            Console.WriteLine($"{total} baju");

        }

        static void soal05()
        {
            Console.WriteLine("---Soal Nomor 5---");
            Console.WriteLine("Sam adalah seorang dosen di universitas dan meberikan nilai seperti ini: ");
            Console.WriteLine("Jika selisih antara nilai dan kelipatan 5 kurang dari 3 , maka nlai merupakan kelipatan 5 tersebut.");
            Console.WriteLine("Jika nilai kurang dari 35 , tidak perlu dibulatan karena hasilnya masih berupa nilai gagal.");
            Console.Write("\n");
            Console.WriteLine("Contoh :");
            Console.WriteLine("nilai= 84 bulat ke kelipatan 5 yitu 85 (85 - 84 kurang dari 3) maka final nilainya adalah 85");
            Console.WriteLine("nilai = 29 jangan dibulatkan (hasilnya kurang dari 35)");
            Console.WriteLine("nilai = 57 dibulatkan ke kelipatan 5 yitu 60 (60 - 57 adalah 3 atau lebih tinggi) maka nilai final tetap 57");
            Console.Write("\n");
            Console.WriteLine("Contoh Output : 73, 67, 38, 33 || 75, 67, 40, 33");
            Console.Write("\n");

            Console.Write("Masukan Nilai = ");
            int nilai = int.Parse(Console.ReadLine());

            if (nilai > 35 && nilai < 100)
            {
                if (nilai % 5 > 2)
                {
                    nilai = nilai + (5 - (nilai % 5));
                }
            }

            Console.WriteLine(nilai);
        }

        static void soal06()
        {
            Console.WriteLine("---Soal Nomor 6---");
            Console.WriteLine("Pangram adalah kata atau kalimat yang mengandung semua elemen alfabet a-z");
            Console.WriteLine("Tentukan apakah contoh kalimat berikut merupakan pangram");
            Console.WriteLine("Input = A quick brown fox jumps over the lazy dog");
            Console.WriteLine("Output = Kalimat ini adalah pangram");
            Console.WriteLine("Input = Check back tomorrow I Will see if the book has arrived");
            Console.WriteLine("Output = Kalimat ini bukan pangram");
            Console.Write("\n");

            Console.Write("Masukan Kalimat = ");
            string kalimat = Console.ReadLine().ToLower();
            char[] kal = kalimat.ToCharArray();
            string az = "abcdefghijklmnopqrstuvwxyz";
            char[] alphabet = az.ToCharArray();
            int hitung = 0;

            //bool pangaram = true;

            for (int i = 0; i < alphabet.Length; i++)
            {
                for (int j = 0; j < kal.Length; j++)
                {
                    if (alphabet[i] == kal[j])
                    {
                        hitung++;
                        break;
                    }

                }
            }
            if (hitung == 26)
            {
                Console.WriteLine("Pangaram");
            }
            else
            {
                Console.WriteLine("Bukan Pangaram");
            }
        }

        static void soal06Ver2()
        {
            Console.WriteLine("---Soal Nomor 6 Ver2---");
            string alphabet = "qwertyuiopasdfghjklzxcvbnm";

            Console.Write("Masukan Kalimat : ");
            string kalimat = Console.ReadLine().ToLower();

            bool pangaram = true;

            foreach (char item in alphabet)
            {
                if (!kalimat.Contains(item))
                {
                    pangaram = false;
                    break;
                }
            }
            if (pangaram)
            {
                Console.WriteLine("Kalimat ini adalah pangaram");
            }
            else
            {
                Console.WriteLine("Kalimat ini bukan pangaram");
            }
        }

        static void soal07()
        {
            Console.WriteLine("---Soal Nomor 7---");
            Console.WriteLine("Buatkanlah 2 buah deret himpunan ganjil dan genap dari deret fibonaci 2 dimana panjang himpunan sesuai inputan.");
            Console.WriteLine("lalu jumlahkan semua dan cari ratanya");
            Console.WriteLine("input : Masukkan maksimal himpunan : 10");
            Console.WriteLine("fibonaci : 1,1,2,3,5,8,13,21,34,55 || Sum = 143 || Avg = 14,3");
            Console.WriteLine("output : Genap : 2,4,6,8,10,12,14,16,18,20 || Sum = 110 || Avg = 11");
            Console.WriteLine("output : Ganjil : 1,3,5,7,9,11,13,15,17,19 || Sum = 100 || Avg = 10");
            Console.Write("\n");

            /*
            Console.Write("Masukan maksimal himpunan = ");
            int himpunan = int.Parse(Console.ReadLine());

            int[] fibonaci = new int[himpunan];
            int genap = 0;
            int ganjil = 1;
            string tampungFibonaci = "";
            string tampungGenap = "";
            string tampungGanjil = "";
            int sumGenap = 0;
            int sumGanjil = 0;
            int sumFibonaci = 0;
            double avgFibonaci = 0;
            double avgGenap = 0;
            double avgGanjil = 0;

            for (int i = 0; i < himpunan; ++i)
            {
                if (i <= 1)
                {
                    fibonaci[i] = 1;
                    tampungFibonaci += fibonaci[i].ToString() + ",";
                    sumFibonaci += fibonaci[i];
                }
                else if (i < himpunan - 1)
                {
                    fibonaci[i] = fibonaci[i - 2] + fibonaci[i - 1];
                    tampungFibonaci += fibonaci[i].ToString() + ",";
                    sumFibonaci += fibonaci[i];
                }
                else
                {
                    fibonaci[i] = fibonaci[i - 2] + fibonaci[i - 1];
                    tampungFibonaci += fibonaci[i].ToString() + "";
                    sumFibonaci += fibonaci[i];
                }
            }

            for (int j = 1; j <= himpunan; j++)
            {
                if (j == himpunan)
                {
                    genap += 2;
                    sumGenap += genap;
                    tampungGenap += genap.ToString();
                }
                else
                {
                    genap += 2;
                    sumGenap += genap;
                    tampungGenap += genap.ToString() + ",";
                }
            }

            for (int k = 1; k <= himpunan; k++)
            {
                if (k == himpunan)
                {
                    tampungGanjil += ganjil.ToString();
                    sumGanjil += ganjil;
                    ganjil += 2;
                }
                else
                {
                    tampungGanjil += ganjil.ToString() + ",";
                    sumGanjil += ganjil;
                    ganjil += 2;
                }
            }
            Console.WriteLine($"Fibonaci : {tampungFibonaci}");
            Console.WriteLine($"Sum Fibonaci : {sumFibonaci}");
            Console.WriteLine($"Genap : {tampungGenap}");
            Console.WriteLine($"Sum Genap : {sumGenap}");
            Console.WriteLine($"Ganjil : {tampungGanjil}");
            Console.WriteLine($"Sum Ganjil : {sumGanjil}");
            Console.WriteLine($"Avg Fibonaci : {sumGenap}");
            Console.WriteLine($"Avg Genap : {tampungGanjil}");
            Console.WriteLine($"Avg Ganjil : {sumGanjil}");
            */

            Console.Write("Masukkan Maksimal himpunan : ");
            int num = int.Parse(Console.ReadLine());

            int[] numArr = new int[num];
            int[] numEven = new int[num];
            int[] numOdd = new int[num];

            int n1 = 2, n2 = 1;
            double res1 = 0, res2 = 0, res3 = 0;

            for (int i = 0; i < num; i++)
            {
                if (i <= 1)
                {
                    numArr[i] = 1;
                }
                else
                {
                    numArr[i] = numArr[i - 2] + numArr[i - 1];
                }

                numEven[i] = n1;
                n1 += 2;

                numOdd[i] = n2;
                n2 += 2;
            }

            for (int i = 0; i < num; i++)
            {
                res1 += numArr[i];
                res2 += numEven[i];
                res3 += numOdd[i];
            }


            Console.WriteLine("Fibonaci : " + string.Join(" ", numArr) + $"\tTotal : {res1}" + $"\tRata-rata : {Math.Round(res1 / numArr.Length, 2)}");
            Console.WriteLine("Genap    : " + string.Join(" ", numEven) + $"\tTotal : {res2}" + $"\tRata-rata : {Math.Round(res2 / numEven.Length, 2)}");
            Console.WriteLine("Ganjil   : " + string.Join(" ", numOdd) + $"\tTotal : {res3}" + $"\tRata-rata : {Math.Round(res3 / numOdd.Length, 2)}");
                    
        }

        static void soal08()
        {
            Console.Write("Masukkan deret angka : ");
            char[] deret = Console.ReadLine().ToCharArray();

            Console.Write("Masukkan angka perulangan : ");
            int ulang = int.Parse(Console.ReadLine());

            int[] numArr = deret.Select(c => Convert.ToInt32(c.ToString())).ToArray();
            int totalP = 0;


            for (int i = 0; i < numArr.Length; i++)
            {
                totalP += numArr[i];
            }

            totalP = totalP * ulang;

            char[] cek = new char[3];
            int[] numArr2 = new int[3];

        loop:
            if (totalP > 10)
            {
                cek = totalP.ToString().ToCharArray();
                numArr2 = cek.Select(c => Convert.ToInt32(c.ToString())).ToArray();
            }
            Console.WriteLine(cek);
            totalP = 0;
            for (int i = 0; i < numArr2.Length; i++)
            {
                totalP += numArr2[i];
            }

            if (totalP > 10)
            {
                goto loop;
            }

            Console.WriteLine(totalP);
        }

        static void soal09()
        {
            Console.WriteLine("---Soal Nomor 9---");
            Console.Write("Masukan beli pulsa = ");
            int pulsa = int.Parse(Console.ReadLine());
            int point1 = 0;
            int point2 = 0;

            if (pulsa <= 10000)
            {
                Console.WriteLine($"0 = 0 Point");
            }
            else if (pulsa <= 30000)
            {
                point1 = (pulsa - 10000) / 1000;
                Console.WriteLine($"0 + {point1} = {point1} Point");
            }
            else
            {
                point1 = 20000 / 1000;
                // point1 = (30000 - 10000) / 1000;
                point2 = ((pulsa - 30000) / 1000) * 2;
                Console.WriteLine($"0 + {point1} + {point2} = {point1 + point2} Point");
            }
        }

        static void soal10()
        {
            Console.Write("Masukkan Nilai Taget : ");
            int target = int.Parse(Console.ReadLine());

            Console.WriteLine();

            Console.Write("Masukkan Nilai Array : ");
            int[] numArr = Array.ConvertAll(Console.ReadLine().Split(" "), int.Parse);

            int count = 0;
            //string tmp = default;

            for (int i = 0; i < numArr.Length; i++)
            {
                for (int j = 0; j < numArr.Length; j++)
                {
                    if (numArr[i] - numArr[j] == target)
                    {
                        count++;
                        //tmp += numArr[i];
                        //tmp += numArr[j];
                    }
                }
            }

            Console.WriteLine();
            Console.WriteLine($"Terdapat {count} pasang dalam array dengan selisih {target}");
        }

        static void soalKisi01()
        {
            //List<int> list = new List<int> ()
            //{
            //    1, 2, 3, 4, 5, 6, 7, 8, 9, 10
            //};

            List<int> list = new List<int>();
            list.Add(1);
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(5);
            list.Add(6);
            list.Add(7);
            list.Add(8);
            list.Add(9);
            list.Add(10);

            for (int i = 0; i < list.Count; i++)
            {
                if (list[i] % 2 == 0)
                {
                    list.RemoveAt(i);
                }
            }
            Console.Write(string.Join(", ", list));

        }

        static void soalKisi02()
        {
            Console.WriteLine("---Soal Kisi 2---");

            Console.Write("Masukan bilangan = ");
            int bilangan = int.Parse(Console.ReadLine());
            bool prima = true;
            int tmp = 0;

            if (bilangan >= 2)
            {
                //for (int i = 2; i <= bilangan; i++)
                //{
                for (int j = 2; j < bilangan; j++)
                {
                    if (bilangan % j == 0)
                    {
                        prima = false;
                        tmp = j;
                        //break;
                    }
                }
                if (prima)
                {
                    Console.WriteLine("1");
                }
                else
                {
                    Console.WriteLine(tmp);
                }
                prima = true;
                //}
            }
            else
            {
                Console.WriteLine("Inputan bukan bilangan prima");
            }

        }

        static void soalRekursif()
        {
            Console.WriteLine("---Soal Rekursif---");
            Console.Write("Masukan Input = ");
            int input = int.Parse(Console.ReadLine());

            //Panggil fungsi
            Perulangan(input);
        }

        static int Perulangan(int input)
        {
            if (input == 0)
            {
                return input;
            }
            Console.WriteLine(input);
            return Perulangan(input - 1);
        }

        static void soalRekursifAsc()
        {
            Console.WriteLine("---Soal Rekursif ASC---");
            Console.Write("Masukan Input = ");
            int input = int.Parse(Console.ReadLine());
            int start = 1;
            //Panggil fungsi
            PerulanganAsc(input, start);
        }

        static int PerulanganAsc(int input, int start)
        {
            if (start == input)
            {
                Console.WriteLine(start);
                return input;
            }
            Console.WriteLine(start);
            return PerulanganAsc(input, start + 1);
        }
    }
}
