﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Xml.Schema;

namespace Logic07_PR
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //soal1();
            //soal1Ver2();
            //soal2();
            //soal2Ver2();
            //soal3();
            //soal4();
            //soal5();
            //soal6();
            //soal7();
            //soal8();
            //soal8Ver2();
            //soal9();
            //soal10();
            //soal11();
            //soal12();
            Console.ReadKey();
        }

        static void soal1()
        {
            Console.WriteLine("---Soal Nomor 1---");
            Console.WriteLine("X orang anak akan duduk bersama di sebuah bangku panjang.");
            Console.WriteLine("Ada berapa cara mereka duduk bersama di bangku tersebut ?");

            Console.Write("Masukan Inputan = ");
            int input = int.Parse(Console.ReadLine());
            
            int[] array = new int[input];

            int sum = 1;

            for (int i = 0; i < input; i++)
            {
                array[i] = i+1;
                //Console.WriteLine(array[i]);
            }
            Console.Write($"{input}! = ");

            for (int j = 0; j < input; j++)
            {
                sum *= array[array.Length - 1] - j;
                int tampung = array[array.Length - 1] - j;
                if (j == input - 1)
                {
                    Console.Write($"{tampung}");        
                }
                else
                {
                    Console.Write($"{tampung} x ");
                }
            }
            Console.WriteLine($" = Ada {sum} cara");
        }

        static void soal1Ver2()
        {
            Console.WriteLine("---Soal Nomor 1---");
            Console.WriteLine("X orang anak akan duduk bersama di sebuah bangku panjang.");
            Console.WriteLine("Ada berapa cara mereka duduk bersama di bangku tersebtu ?");

            Console.Write("Masukan Inputan = ");
            int input = int.Parse(Console.ReadLine());
            int kali = 1;

            string angka = "";

            Console.Write($"!{input} = ");

            for (int i = input; i > 0; i--)
            {
                kali *= i;

                angka += angka == "" ? i.ToString() : " x " + i.ToString();

                //if (i == input)
                //{   
                //    Console.WriteLine($" {input - i + 1} = {kali}");
                //}
                //else
                //{
                //    Console.Write($" {input - i + 1} x");
                //}
            }
            Console.WriteLine($"{angka} = {kali}");
            Console.WriteLine($"Ada {kali} cara");
        }

        static void soal2()
        {
            Console.WriteLine("---Soal Nomor 2---");
            Console.Write("Masukan Sinyal(12 huruf) = ");
            string kalimat = Console.ReadLine().ToUpper();
            char[] huruf = kalimat.ToCharArray();

            int jumlahBenar = 0;
            int jumlahSalah = 0;

            if (kalimat.Length == 12)
            {

                for (int i = 0; i < kalimat.Length; i += 3)
                {
                    if (huruf[i] != 'S')
                    {
                        jumlahSalah += 1;
                    }

                    if (huruf[i + 1] != 'O')
                    {
                        jumlahSalah += 1;
                    }

                    if (huruf[i + 2] != 'S')
                    {
                        jumlahSalah += 1;
                    }

                    if (huruf[i] == 'S' && huruf[i + 1] == 'O' && huruf[i + 2] == 'S')
                    {
                        jumlahBenar += 1;
                    }
                }
                Console.WriteLine($"Jumlah sinyal yang benar = {jumlahBenar}");
                Console.WriteLine($"Jumlah sinyal salah = {jumlahSalah}");
            }
            else
            {
                Console.WriteLine("Inputan harus 12");
            }
        }

        static void soal2Ver2()
        {
            Console.Write("Masukan Signal = ");
            char[] sinyal = Console.ReadLine().ToUpper().ToCharArray();

            int count = 0;
            int countb = 0;
            string tmp = "";
            if (sinyal.Length % 3 != 0)
            {
                Console.WriteLine("Invalid, Masukan kode sinyal yang benar");
            }
            else
            {
                for (int i = 0; i < sinyal.Length; i += 3)
                {
                    if (sinyal[i] != 'S' || sinyal[i + 1] != 'O' || sinyal[i + 2] != 'S')
                    {
                        count++;
                        tmp += "SOS";
                    }
                    else
                    {
                        tmp += sinyal[i].ToString() + sinyal[i + 1].ToString() + sinyal[i + 2].ToString();
                        countb++;
                    }
                }
            }
            Console.WriteLine();
            Console.WriteLine($"Total sinyal yang salah adalah = {count}");
            Console.WriteLine($"Total sinyal yang benar adalah = {countb}");
            Console.WriteLine($"Sinyal yang diterima = {string.Join("", sinyal)}");
            Console.WriteLine($"Sinyal yang benar = {tmp}");
        }

        static void soal3()
        {
            Console.WriteLine("---Soal Nomor 3---");
            Console.Write("Masukan Tanggal Peminjaman Buku(dd/mm/yyyy) = ");
            string tanggalMasuk = Console.ReadLine();
            Console.Write("Masukan Tanggal Pengembalian Buku(dd/mm/yyyy) = ");
            string tanggalPengembalian = Console.ReadLine();
            
            DateTime tglMasuk = DateTime.Parse(tanggalMasuk);
            DateTime tglPengembalian = DateTime.Parse(tanggalPengembalian);
            
            TimeSpan hasil = tglPengembalian - tglMasuk;
            int hasil1 = hasil.Days;
            
            int hasil2 = (hasil1 - 3) * 500;
            Console.WriteLine($"Denda telat pengembalian = {hasil2}");
        }

        static void soal4()
        {
            Console.Write("Masukan tanggal mulai (dd/mm/yyyy) = ");
            DateTime dateStart = DateTime.Parse(Console.ReadLine());
            Console.Write("Masukan berapa lagi akan ujian = ");
            int exam = int.Parse(Console.ReadLine());
            Console.Write("Masukan tanggal libur = ");
            int[] holiday = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);

            DateTime tanggalSelesai = dateStart.AddDays(-1);

            for (int i = 0; i < exam; i++)
            {
                tanggalSelesai = tanggalSelesai.AddDays(1);
                if (tanggalSelesai.DayOfWeek == DayOfWeek.Saturday)
                {
                    tanggalSelesai = tanggalSelesai.AddDays(2);
                }

                for (int j = 0; j < holiday.Length; j++)
                {
                    if (tanggalSelesai.Day == holiday[j])
                    {
                        tanggalSelesai = tanggalSelesai.AddDays(1);

                        if (tanggalSelesai.DayOfWeek == DayOfWeek.Saturday)
                        {
                            tanggalSelesai = tanggalSelesai.AddDays(2);
                        }
                    }
                }
            }

            DateTime tanggalUjian = tanggalSelesai.AddDays(1);
            if (tanggalUjian.DayOfWeek == DayOfWeek.Saturday)
            {
                tanggalUjian = tanggalUjian.AddDays(2);
            }
            Console.Write("\n");
            Console.WriteLine($"Kelas akan ujian pada tanggal = " + tanggalUjian.ToString("dddd, dd/MM/yyyy"));

            /*Console.WriteLine("---Soal Nomor 4---");
            Console.Write("Masukan Tanggal Mulai(dd/mm/yyyy) = ");
            string tanggalMulai = Console.ReadLine();
            Console.Write("Masukan Tanggal Hari libur(dipisahkan dengan koma) = ");
            string tanggalLibur = Console.ReadLine();

            string[] hariLibur = tanggalLibur.Split(",");
            int jumlahLibur = hariLibur.Length;

            DateTime tgl1 = DateTime.Parse(tanggalMulai);

            int sabtuMinggu = 2;
            int jmlSabtuMinggu = (sabtuMinggu * 3) ;
            DateTime tgl2 = tgl1.AddDays(10 + jumlahLibur + jmlSabtuMinggu);

            Console.WriteLine(tgl2);

            //Console.WriteLine(jumlahLibur);*/
        }

        static void soal5()
        {
            Console.WriteLine("---soal1 Nomor 5---");
            Console.Write("Masukan Kalimat = ");
            string kalimat = Console.ReadLine().ToUpper();
            string[] perKata = kalimat.Split(" ");

            int hitung1 = 0;
            int hitung2 = 0;

            for (int i = 0; i < perKata.Length; i++)
            {
                char[] huruf = perKata[i].ToCharArray();

                for (int j = 0; j < perKata[i].Length; j++)
                {
                    if (huruf[j] == 'A')
                    {
                        hitung1++;
                    }
                    else if (huruf[j] == 'I')
                    {
                        hitung1++;
                    }
                    else if (huruf[j] == 'U')
                    {
                        hitung1++;
                    }
                    else if (huruf[j] == 'E')
                    {
                        hitung1++;
                    }
                    else if(huruf[j] == 'O')
                    {
                        hitung1++;
                    }
                    else
                    {
                        hitung2++;
                    }
                }
            }
            Console.WriteLine($"Jumlah Huruf Vokal : {hitung1}");
            Console.WriteLine($"Jumlah Huruf Konsonan : {hitung2}");
        }

        static void soal6()
        {
            Console.WriteLine("---Soal Nomor 6---");
            Console.Write("Masukan Kalimat = ");
            char[] kalimat = Console.ReadLine().ToLower().Replace(" ","").ToCharArray();

            for (int i = 0; i < kalimat.Length; i++)
            {
                Console.WriteLine($"***{kalimat[i]}***");
            }
        }

        static void soal7()
        {
            Console.WriteLine("---Soal Nomor 7---");
            Console.Write("Jumlah Makanan Yang Di Pesan = ");
            int jmlMakanan = int.Parse(Console.ReadLine());
            Console.Write("Index Makanan Yang Tidak Di Makan Elsa = ");
            int indexMakanan = int.Parse(Console.ReadLine());
            Console.Write("Jumlah Harga Setiap Menu Makanan(Pisahkan dengan koma) = ");
            int[] menuMakanan = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            Console.Write("Masukan Uang Elsa = ");
            int uangElsa = int.Parse(Console.ReadLine());
            int totalMakan = 0;

            for (int i = 0; i < menuMakanan.Length; i++)
            {
                totalMakan += menuMakanan[i];
            }

            int total = totalMakan - menuMakanan[indexMakanan];
            int totalBayar2 = total / 2;

            int sisa = uangElsa - totalBayar2;

            Console.WriteLine($"Elsa harus membayar = {totalBayar2}");

            if (sisa > 0 )
            {
                Console.WriteLine($"Sisa uang elsa = {sisa.ToString("Rp. #,##0.0")}");
            }
            else if (sisa == 0)
            {
                Console.WriteLine("Uang Pas");
            }
            else
            {
                Console.WriteLine($"Uang elsa kurang = {sisa.ToString("#,##0.0")}");
            }

        }

        static void soal8()
        {
            Console.WriteLine("---Soal Nomor 8---");
            Console.Write("Masukan Input = ");
            int input = int.Parse(Console.ReadLine()); //5
            
            for (int i = 1; i <= input; i++) // i = 1 <= 5 || i = 2 <= 5 ||
            {
                for (int j = input - i; j >= 1; j--) // j = 4, 3, 2, 1 || j = 3, 2, 1 
                {
                    Console.Write(" "); // "", "", "", "", || "", "", ""
                }

                for (int k = 1; k <= i; k++) // k = 1 <= 1  || k = 1 < 2, 1
                {
                    Console.Write("*"); // "*" || "*", "*"
                }

                Console.Write("\n");
            }
            
        }

        static void soal8Ver2()
        {
            Console.WriteLine("---Soal Nomor 8---");
            Console.Write("Masukan Input = ");
            int input = int.Parse(Console.ReadLine());

            for (int i = 0; i < input; i++)
            {
                for (int j = 0; j < input; j++)
                {
                    if (j < input - 1 - i)
                    {
                        Console.Write(" ");
                    }
                    else
                    {
                        Console.Write("*");
                    }
                }
                Console.Write("\n");
            }
        }

        static void soal9()
        {
            Console.WriteLine("---Soal Nomor 9---");

            int[,] matrix = new int[,]
            {
                {11, 2, 4},
                {4, 5, 6},
                {10, 8, -12}
            };

            string tampungDiagonal1 = "";

            int primaryDiagonalSum = 0;

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                primaryDiagonalSum += matrix[i, i];
                //tampungDiagonal1 += tampungDiagonal1 == "" ? matrix[i, i].ToString() : matrix[i,i] < 0 ? " - " + Math.Abs(matrix[i, i]).ToString() :  " + " + matrix[i, i].ToString();
                if (tampungDiagonal1 == "")
                {
                    tampungDiagonal1 += matrix[i, i].ToString();
                }
                else if (matrix[i, i] < 0)
                {
                    tampungDiagonal1 += " - " + Math.Abs(matrix[i, i]).ToString();
                }
                else
                {
                    tampungDiagonal1 += " + " + matrix[i, i].ToString();
                }
            }

            string tampungDiagonal2 = "";
            int secondaryDiagonalSum = 0;

            for (int i = 0; i < matrix.GetLength(0); i++)
            {
                secondaryDiagonalSum += matrix[i, matrix.GetLength(0) - 1 - i];
                tampungDiagonal2 += tampungDiagonal2 == "" ? matrix[i, matrix.GetLength(0) - 1 - i].ToString() : " + " + matrix[i, matrix.GetLength(0) - 1 - i].ToString();
            }

            int diagonalDiffrence = primaryDiagonalSum - secondaryDiagonalSum;

            

            Console.WriteLine($"Diagonal 1 : {tampungDiagonal1} = {primaryDiagonalSum}");
            Console.WriteLine($"Diagonal 2 : {tampungDiagonal2} = {secondaryDiagonalSum}");
            Console.WriteLine($"Perbedaan Diagonal = {primaryDiagonalSum} - {secondaryDiagonalSum} = {diagonalDiffrence}");
        }

        static void soal10()
        {
            Console.WriteLine("---Soal Nomor 10---");
            Console.Write("Masukan sampel input(Pakai Koma)= ");
            int[] input = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse);
            int nilaiMax = 0;
            int hitung = 0;

            for (int i = 0; i < input.Length; i++)
            {

                if (input[i] >= nilaiMax)
                {
                    nilaiMax = input[i];
                }
            }

            for (int j = 0; j < input.Length; j++)
            {
                if (input[j] == nilaiMax)
                {
                    hitung += 1;
                }
            }

            //for (int j = 0; j < input.Length; j++)
            //{
            //    if (input[j] >= nilaiMax)
            //    {
            //        nilaiMax = input[j];
            //    }
            //    if (input[j] == nilaiMax)
            //    {
            //        hitung += 1;
            //    }
            //}

            Console.WriteLine(hitung);
        }

        static void soal11()
        {
            Console.Write("Masukkan input angka (pakai koma) = ");
            int[] arr = Array.ConvertAll(Console.ReadLine().Split(","), int.Parse); // 5,6,7,0,1 || 6, 7, 0, 1, 5 
            Console.Write("Masukkan rotasi = ");
            int rotasi = int.Parse(Console.ReadLine());

            for (int i = 0; i < rotasi; i++)
            {
                int tampung = arr[0]; // tampung 5, tampung 6,
                for (int j = 0; j < arr.Length - 1; j++)
                {
                    arr[j] = arr[j + 1]; // arr[0] = 6, 7, 0, 1 
                }
                arr[arr.Length - 1] = tampung; //arr[4] = 5
            }

            Console.Write($"Rotasi {rotasi} = ");
            for (int i = 0; i < arr.Length; i++)
            {
                Console.Write(arr[i]);
                if (i != arr.Length - 1)
                {
                    Console.Write(",");
                }
            }
        }

        static void soal12()
        {
            Console.WriteLine("---Soal Nomor 12---");
            Console.WriteLine();
            Console.Write("Masukkan banyak deret : ");
            int deret = int.Parse(Console.ReadLine());
            Console.WriteLine();

            int[] angka = new int[deret];
            int x = 0;
            int no = 1;

            while (x < deret)
            {
                Console.Write($"Masukkan data ke-{no++} : ");
                angka[x] = int.Parse(Console.ReadLine());
                x++;
            }
            Console.Write("\n");

            int tmp = 0;

            for (int i = 0; i < angka.Length; i++) //2,5,4,1,3
            {
                for (int j = 0; j < angka.Length - 1; j++) // 2,5,4,1,3 || 2,4,5,1,3 

                    if (angka[j] > angka[j + 1])
                    {
                        tmp = angka[j]; // tmp = 5 
                        angka[j] = angka[j + 1]; // angka[1] = 4
                        angka[j + 1] = tmp; // angka [2] = 5
                    }
                Console.WriteLine(string.Join(",", angka));

            }

            Console.Write("\n");

            foreach (int item in angka)
            {
                Console.Write(item+ " ");
            }
        }
    }
}
