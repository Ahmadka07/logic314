CREATE DATABASE DB_HR
USE DB_HR

CREATE TABLE tb_karyawan(
id BIGINT PRIMARY KEY IDENTITY(1,1) NOT NULL,
nip VARCHAR(50) NOT NULL,
nama_depan VARCHAR(50) NOT NULL,
nama_belakang VARCHAR(50) NOT NULL,
jenis_kelamin VARCHAR(50) NOT NULL,
agama VARCHAR(50) NOT NULL,
tempat_lahir VARCHAR(50) NOT NULL,
tgl_lahir DATE,
alamat VARCHAR(100) NOT NULL,
pendidikan_terakhir VARCHAR(50) NOT NULL,
tgl_masuk DATE
)

INSERT INTO tb_karyawan (nip, nama_depan, nama_belakang, jenis_kelamin, agama, tempat_lahir, tgl_lahir, alamat, pendidikan_terakhir, tgl_masuk)
VALUES
('001', 'Hamidi', 'Samsudin', 'Pria', 'Islam', 'Sukabumi', '1997-04-21', 'Jl Sudirman No.12', 'S1 Teknik Mesin', '2015-12-07'),
('003', 'Paul', 'Chirstian', 'Pria', 'Kristen', 'Ambon', '1980-05-27', 'Jl Veteran No.4', 'S1 Pendidikan Geografi', '2014-01-12'),
('002', 'Ghandi', 'Wamida', 'Wanita', 'Islam', 'Palu', '1992-01-12', 'Jl Rambutan No.22', 'SMA Negeri 02 Palu', '2014-12-01')

SELECT * FROM tb_karyawan

CREATE TABLE tb_divisi(
id BIGINT PRIMARY KEY IDENTITY(1,1) NOT NULL,
kd_divisi VARCHAR(50) NOT NULL,
nama_divisi VARCHAR(50) NOT NULL
)

INSERT INTO tb_divisi (kd_divisi, nama_divisi)
VALUES
('GD', 'Gudang'),
('HRD', 'HRD'),
('KU', 'Keuangan'),
('UM', 'Umum')

SELECT * FROM tb_divisi

CREATE TABLE tb_jabatan (
id BIGINT PRIMARY KEY IDENTITY(1,1) NOT NULL,
kd_jabatan VARCHAR(50) NOT NULL,
nama_jabatan VARCHAR(50) NOT NULL,
gaji_pokok NUMERIC,
tunjangan_jabatan NUMERIC
)

INSERT INTO tb_jabatan(kd_jabatan, nama_jabatan, gaji_pokok, tunjangan_jabatan)
VALUES
('MGR', 'Manager', 5500000, 1500000),
('OB', 'Office Boy', 1900000, 200000),
('ST', 'Staff', 3000000, 750000),
('WMGR', 'Wakil Manager', 4000000, 1200000)

SELECT * FROM tb_jabatan

CREATE TABLE tb_pekerjaan(
id BIGINT PRIMARY KEY IDENTITY(1,1) NOT NULL,
nip VARCHAR(50) NOT NULL,
kode_jabatan VARCHAR(50) NOT NULL,
kode_divisi VARCHAR(50) NOT NULL,
tunjangan_kinerja NUMERIC,
kota_penempatan VARCHAR(50)
)

INSERT INTO tb_pekerjaan(nip, kode_jabatan, kode_divisi, tunjangan_kinerja, kota_penempatan)
VALUES
('001', 'ST', 'KU', 750000, 'Cianjur'),
('002', 'OB', 'UM', 350000,'Sukabumi'),
('003', 'MGR', 'HRD', 1500000, 'Sukabumi')

SELECT * FROM tb_pekerjaan

--SOAL NOMOR 1 (Tampilkan nama lengkap, nama jabatan, tunjangan jabatan + gaji, yang gaji + tunjangan kinerja dibawah 5juta)
SELECT (tb_karyawan.nama_depan + ' ' +tb_karyawan.nama_belakang) AS nama_lengkap, tb_jabatan.nama_jabatan, tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan AS gaji_tunjangan
FROM tb_karyawan
JOIN tb_pekerjaan ON tb_karyawan.nip = tb_pekerjaan.nip
JOIN tb_jabatan ON tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
WHERE tb_jabatan.gaji_pokok + tb_pekerjaan.tunjangan_kinerja < 5000000

--SOAL NOMOR 2 (Tampilkan nama lengkap, nama jabatan, nama divisi, total gaji, pajak, gaji bersih yang gender pria dan penempatan kerjanya di luar sukabumi)
SELECT CONCAT(tb_karyawan.nama_depan, ' ', tb_karyawan.nama_belakang) AS nama_lengkap, tb_jabatan.nama_jabatan,
tb_divisi.nama_divisi, tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja AS total_gaji, 
(tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) * 0.05 AS pajak,
(tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) - ((tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) * 0.05) 
AS gaji_bersih
FROM tb_karyawan
JOIN tb_pekerjaan ON tb_karyawan.nip = tb_pekerjaan.nip
JOIN tb_divisi ON tb_pekerjaan.kode_divisi = tb_divisi.kd_divisi
JOIN tb_jabatan ON tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
WHERE tb_karyawan.jenis_kelamin = 'Pria' AND tb_pekerjaan.kota_penempatan != 'Sukabumi'

--SOAL NOMOR 3 (Tampilkan nip, nama lengkap, nama jabatan, nama divisi, bonus(bonus = 25% dari total gaji(gaji pokok + tunjangan_jabatan + tunjangan_kinerja)) * 7)
SELECT tb_karyawan.nip ,CONCAT(tb_karyawan.nama_depan, ' ', tb_karyawan.nama_belakang) AS nama_lengkap, tb_jabatan.nama_jabatan,
tb_divisi.nama_divisi,
(tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) * 0.25 * 7 AS bonus
FROM tb_karyawan
JOIN tb_pekerjaan ON tb_karyawan.nip = tb_pekerjaan.nip
JOIN tb_divisi ON tb_pekerjaan.kode_divisi = tb_divisi.kd_divisi
JOIN tb_jabatan ON tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan

--SOAL NOMOR 4 (Tampilkan  NIP, nama lengkap, total gaji, infak(5% dari total gaji) yang mempunyai jabatan manager)
SELECT tb_karyawan.nip ,CONCAT(tb_karyawan.nama_depan, ' ', tb_karyawan.nama_belakang) AS nama_lengkap, tb_jabatan.nama_jabatan,
tb_divisi.nama_divisi,
tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja AS total_gaji,
(tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) * 0.05 AS infak
FROM tb_karyawan
JOIN tb_pekerjaan ON tb_karyawan.nip = tb_pekerjaan.nip
JOIN tb_divisi ON tb_pekerjaan.kode_divisi = tb_divisi.kd_divisi
JOIN tb_jabatan ON tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
WHERE tb_jabatan.kd_jabatan = 'mgr'

--SOAL NOMOR 5 (Tampilkan NIP, nama lengkap,  nama jabatan, pendidikan terakhir, tunjangan pendidikan(2jt), dan total gaji(gapok + tunjangan jabatan + tunjangan pendidikan dimana pendidikan akhirnya s1))
SELECT tb_karyawan.nip ,CONCAT(tb_karyawan.nama_depan, ' ', tb_karyawan.nama_belakang) AS nama_lengkap, tb_jabatan.nama_jabatan,
tb_karyawan.pendidikan_terakhir, 2000000 AS total_gaji,
tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + 2000000 AS total_gaji
FROM tb_karyawan
JOIN tb_pekerjaan ON tb_karyawan.nip = tb_pekerjaan.nip
JOIN tb_divisi ON tb_pekerjaan.kode_divisi = tb_divisi.kd_divisi
JOIN tb_jabatan ON tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
WHERE tb_karyawan.pendidikan_terakhir LIKE '%S1%'
ORDER BY tb_karyawan.nama_depan ASC

--SOAL NOMOR 6(Tampilkan NIP, nama lengkap, nama jabatan, nama divisi, bonus
			 --MGR = (bonus = 25% dari total gaji * 7)
			 --ST = (bonus = 25% dari total gaji * 5)
			 --OTHER = (bonus = 25% dari total gaji * 2))
SELECT tb_karyawan.nip ,CONCAT(tb_karyawan.nama_depan, ' ', tb_karyawan.nama_belakang) AS nama_lengkap, 
tb_jabatan.nama_jabatan, tb_divisi.nama_divisi,
CASE 
	WHEN tb_pekerjaan.kode_jabatan = 'MGR' THEN 
		 (tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) * 0.25 * 7
	WHEN tb_pekerjaan.kode_jabatan = 'ST' THEN 
		 (tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) * 0.25 * 5
	ELSE (tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) * 0.25 * 2
END AS bonus
FROM tb_karyawan
JOIN tb_pekerjaan ON tb_karyawan.nip = tb_pekerjaan.nip
JOIN tb_divisi ON tb_pekerjaan.kode_divisi = tb_divisi.kd_divisi
JOIN tb_jabatan ON tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
ORDER BY tb_karyawan.nip ASC

--SOAL NOMOR 7 (Buatlah kolom nip pada tabel karyawan sebagai kolom unique)
ALTER TABLE tb_karyawan ADD CONSTRAINT unique_nip UNIQUE(nip)

--SOAL NOMOR 8 (Buatlah kolom nip pada tabel karyawan sebagai index)
CREATE INDEX index_nip ON tb_karyawan(nip)

--SOAL NOMOR 9 (Tampilkan nama lengkap, nama belakangnya diubah menjadi huruf capital dengan kondisi nama belakang diawali dengan huruf W)
SELECT CONCAT(tb_karyawan.nama_depan, ' ', tb_karyawan.nama_belakang) AS nama_lengkap, UPPER(tb_karyawan.nama_belakang) AS nama_belakang
FROM tb_karyawan
WHERE nama_belakang LIKE 'W%'

--SOAL NOMOR 10 ()
SELECT CONCAT(tb_karyawan.nama_depan, ' ', tb_karyawan.nama_belakang) AS nama_lengkap, tb_karyawan.tgl_masuk, tb_jabatan.nama_jabatan,
tb_divisi.nama_divisi,
tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja AS total_gaji, 
(tb_jabatan.gaji_pokok + tb_jabatan.tunjangan_jabatan + tb_pekerjaan.tunjangan_kinerja) * 0.10 AS bonus, 
DATEDIFF(YEAR, tb_karyawan.tgl_masuk, GETDATE()) AS total_gaji
FROM tb_karyawan
JOIN tb_pekerjaan ON tb_karyawan.nip = tb_pekerjaan.nip
JOIN tb_divisi ON tb_pekerjaan.kode_divisi = tb_divisi.kd_divisi
JOIN tb_jabatan ON tb_pekerjaan.kode_jabatan = tb_jabatan.kd_jabatan
WHERE DATEDIFF(YEAR, tb_karyawan.tgl_masuk, GETDATE()) >= 8








