--DDL

--CREATE DATABASE

/*create database db_kampus*/
CREATE DATABASE db_kampus
CREATE DATABASE db_kampus2

USE db_kampus

--CREATE TABLE
CREATE TABLE mahasiswa(
id BIGINT PRIMARY KEY IDENTITY(1,1),
name VARCHAR (50) NOT NULL,
address VARCHAR (50) NOT NULL,
email VARCHAR (255) NOT NULL,
)

CREATE TABLE mahasiswa2(
id BIGINT PRIMARY KEY IDENTITY(1,1),
[primary] VARCHAR (50) NOT NULL,
address VARCHAR (50) NOT NULL,
email VARCHAR (255) NOT NULL,
)

--CREATE VIEW
CREATE VIEW vwMahasiswa
AS --querynya disini
SELECT id, name, address, email FROM mahasiswa

--SELECT VIEW
SELECT * FROM vwMahasiswa

--DROP VIEW
DROP VIEW vwMahasiswa

--ALTER ADD COLUMN
ALTER TABLE mahasiswa ADD [description] VARCHAR (255)

--ALTER DROP COLUMN 
ALTER TABLE mahasiswa DROP COLUMN [description]

--TABLE ALTER COLUMN
ALTER TABLE mahasiswa ALTER COLUMN email VARCHAR (100)

--ALTER VIEW RENAME
--ALTER VIEW [NamaViewLama] TO [NamaViewBaru]

-------------------------------------------------------

--DROP DATABASE
DROP DATABASE db_kampus2

--DROP TABLE
DROP TABLE mahasiswa2

-------------------------------------------------------

--DML

--INSERT DATA
INSERT INTO mahasiswa (
name, address, email
)
VALUES (
'Marchel', 'Medan', 'marchelajadeh@gmail.com'
)

INSERT INTO mahasiswa (
name, address, email
)
VALUES (
'Toni', 'Garut', 'toni@gmail.com'
)

INSERT INTO mahasiswa (
name, address, email
)
VALUES (
'Isni', 'Cimahi', 'isni@gmail.com'
)

INSERT INTO mahasiswa (
name, address, email
)
VALUES 
('Anwar', 'Ragunan', 'anwar@gmail.com'),
('Alfi', 'Medan', 'alfi@gmail.com')

--SELECT DATA
SELECT id, name, address FROM mahasiswa
SELECT * FROM mahasiswa

--UPDATE DATA
UPDATE mahasiswa SET
name = 'Isni Dwitiniardi',
address = 'Sumedang'
WHERE id = 3

--DELETE DATA
DELETE mahasiswa WHERE name = 'Alfi'
DELETE mahasiswa WHERE name = 'Anwar'
DELETE mahasiswa WHERE name = 'Toni'

CREATE TABLE Biodata (
id BIGINT PRIMARY KEY IDENTITY (1,1),
dob DATETIME NOT NULL,
kota VARCHAR (100) NULL
)

ALTER TABLE Biodata ADD mahasiswa_id BIGINT
ALTER TABLE Biodata ALTER COLUMN mahasiswa_id BIGINT NOT NULL

--JOIN TABLE
SELECT * FROM mahasiswa AS mhs
JOIN Biodata AS bio ON mhs.id = bio.mahasiswa_id
WHERE mhs.name = 'Toni'

SELECT mhs.id AS ID, mhs.name, bio.kota, bio.dob,
MONTH(bio.dob) BulanLahir, YEAR(bio.dob) TahunLahir 
FROM mahasiswa AS mhs
JOIN Biodata AS bio ON mhs.id = bio.mahasiswa_id
WHERE mhs.name = 'Toni'


INSERT INTO Biodata (
dob, kota, mahasiswa_id
)
VALUES
( '2000/10/12', 'Medan', 1),
( '2000/09/12', 'Sumedang', 3),
( '2000/08/12', 'Garut', 6)

ALTER TABLE Biodata ALTER COLUMN dob DATE NOT NULL

SELECT * FROM Biodata

--AND , OR
SELECT mhs.id AS ID, mhs.name, bio.kota, bio.dob,
MONTH(bio.dob) BulanLahir, YEAR(bio.dob) TahunLahir 
FROM mahasiswa AS mhs
JOIN Biodata AS bio ON mhs.id = bio.mahasiswa_id
WHERE mhs.name = 'Toni' AND bio.kota = 'jakarta'

SELECT mhs.id AS ID, mhs.name, bio.kota, bio.dob,
MONTH(bio.dob) BulanLahir, YEAR(bio.dob) TahunLahir 
FROM mahasiswa AS mhs
JOIN Biodata AS bio ON mhs.id = bio.mahasiswa_id
WHERE mhs.name = 'Toni' OR bio.kota = 'jakarta'

--ORDER BY
SELECT *
FROM mahasiswa mhs
JOIN Biodata bio on mhs.id = bio.mahasiswa_id
ORDER BY mhs.id asc, mhs.name desc

--SELECT TOP
SELECT TOP 1 * FROM mahasiswa ORDER BY name DESC
SELECT TOP 1 * FROM mahasiswa ORDER BY name ASC
SELECT TOP 1 * FROM mahasiswa

--BETWEEN
SELECT * FROM mahasiswa WHERE id BETWEEN 1 AND 3
SELECT * FROM mahasiswa WHERE id >= 1 AND id <= 3

-- LIKE
SELECT * FROM mahasiswa WHERE name LIKE 'm%'
SELECT * FROM mahasiswa WHERE name LIKE '%i'
SELECT * FROM mahasiswa WHERE name LIKE '%dWi%'
SELECT * FROM mahasiswa WHERE name LIKE '_o%'
SELECT * FROM mahasiswa WHERE name LIKE '__n%'
SELECT * FROM mahasiswa WHERE name LIKE 't_%'
SELECT * FROM mahasiswa WHERE name LIKE 't___%'
SELECT * FROM mahasiswa WHERE name LIKE 'i%i'

--GROUP BY
SELECT name FROM mahasiswa GROUP BY name
SELECT sum(id), name FROM mahasiswa GROUP BY name

--HAVING
SELECT COUNT(id), name
FROM mahasiswa
GROUP BY name
HAVING COUNT(id) > 1

--DISTINCT
SELECT DISTINCT name FROM mahasiswa

--SUBSTRING
SELECT SUBSTRING ('SQL Tutorial', 1, 3) AS JUDUL

--CHARINDEX
SELECT CHARINDEX ('t', 'Customer') AS JUDUL

--DATALENGTH
SELECT DATALENGTH ('Akumau.istirahat') as JUDUl

ALTER TABLE mahasiswa ADD panjang SMALLINT

SELECT * FROM mahasiswa

UPDATE mahasiswa SET panjang = 48 WHERE id = 1
UPDATE mahasiswa SET panjang = 117 WHERE id = 3
UPDATE mahasiswa SET panjang = 86 WHERE id = 6
UPDATE mahasiswa SET panjang = 50 WHERE id = 7

--CASE
SELECT id, name, address, panjang, 
CASE 
	WHEN panjang < 50 THEN 'Pendek'
	WHEN panjang <= 100 THEN 'Sedang'
	ELSE 'Tinggi'
END AS TinggiBadan
FROM mahasiswa

--CONCAT
SELECT CONCAT ('SQL ', 'IS ', 'FUN!')
SELECT 'SQL ' + 'IS ' + 'FUN!' AS JUDUL

--OPERATORARITMATIKA
CREATE TABLE penjualan (
id BIGINT PRIMARY KEY IDENTITY (1,1),
nama VARCHAR (50) NOT NULL,
harga int NOT NUll
)

INSERT INTO penjualan (nama, harga)
VALUES
	('Indomie', 1500), ('Close-Up', 3500), ('Pepsodent', 3000),
	('Brush Formula', 2500), ('Roti Manis', 1000), ('Gula', 3500),
	('Sarden', 4500), ('Rokok Sampoerna', 11000), ('Rokok 234', 11000)

SELECT nama, harga, harga * 100 AS harga_x_100 FROM penjualan 

SELECT * FROM penjualan

USE db_kampus

SELECT *, ROW_NUMBER() OVER(ORDER BY nama) as nomor_urut FROM penjualan

SP_COLUMNS 'penjualan'
SP_HELP 'penjualan'
